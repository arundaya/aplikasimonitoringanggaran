<?php
class ProgramModel extends CI_Model
{
    function getPosts(){
    $this->db->query("DROP TABLE IF EXISTS tmp_satk");
    $this->db->query("CREATE TEMPORARY TABLE tmp_satk SELECT DISTINCT KDSATKER,THANG,KDDEPT,KDUNIT,KDDEKON FROM D_ITEM");

     
    $this->db->distinct();
    $this->db->select("a.kdsatker, nmsatker,a.kddept,a.kdunit, a.kddekon, nmdekon2 nmdekon, a.thang FROM tmp_satk a LEFT JOIN t_satker b ON a.kdsatker = b.kdsatker LEFT JOIN t_dekon c ON a.kddekon= c.kddekon"); 
    //$this->db->from('tmp_satk a');
    //$this->db->join('left', 'a.kdsatker=b.kdsatker');
    $query = $this->db->get();
    return $query->result();
 }
 
 function getDataRKAKL(){
    //RKA 2015
    $this->db->query("DROP TABLE IF EXISTS z_item");
    $this->db->query("CREATE TEMPORARY TABLE z_item SELECT THANG, KDJENDOK, KDSATKER, KDDEPT, KDUNIT, KDPROGRAM, KDGIAT, KDOUTPUT, KDLOKASI, KDKABKOTA, KDDEKON, 
    IFNULL(KDSOUTPUT, '000') as KDSOUTPUT, KDKMPNEN, KDSKMPNEN, KDAKUN, KDKPPN, KDBEBAN, KDJNSBAN, KDCTARIK, REGISTER, CARAHITUNG, HEADER1, 
    HEADER2, KDHEADER, NOITEM, NMITEM, VOL1, SAT1, VOL2, SAT2, VOL3, SAT3, VOL4, SAT4, VOLKEG, SATKEG, 
    HARGASAT, JUMLAH, JUMLAH2, PAGUPHLN, PAGURMP, PAGURKP, KDBLOKIR, BLOKIRPHLN, BLOKIRRMP, BLOKIRRKP, 
    RPHBLOKIR, KDCOPY, KDABT, KDSBU, VOLSBK, VOLRKAKL, BLNKONTRAK, NOKONTRAK, TGKONTRAK, NILKONTRAK, JANUARI, PEBRUARI, 
    MARET, APRIL, MEI, JUNI, JULI, AGUSTUS, SEPTEMBER, OKTOBER, NOPEMBER, DESEMBER, JMLTUNDA, KDLUNCURAN, JMLABT, NOREV, 
    KDUBAH, KURS, INDEXKPJM, KDIB
    FROM d_item WHERE kdsatker = 562103 AND thang=2015;");
    
    $this->db->query("DROP TABLE IF EXISTS z_item_output");
    $this->db->query("CREATE TEMPORARY TABLE z_item_output SELECT CONCAT(a.kddept,a.kdunit,a.kdprogram,a.kdgiat,a.kdoutput,a.kdlokasi,a.kdkabkota,a.kddekon,a.kdib) kdindex, CONCAT(a.kddept,a.kdunit,a.kdprogram) as kdbreak, CONCAT_WS('.', a.kdgiat,a.kdoutput) as kode, SUM(jumlah) as jumlah, 3 as lvl, CONCAT(a.kdgiat,a.kdoutput) as field1 FROM z_item a GROUP BY kdindex;");

    $this->db->query("DROP TABLE IF EXISTS tbl_output");
    $this->db->query("CREATE TEMPORARY TABLE tbl_output SELECT *, CONCAT(kdgiat, kdoutput) as field2 from t_output");
    
     $this->db->query("DROP TABLE IF EXISTS tbl_fin1");
     $this->db->query("CREATE TEMPORARY TABLE tbl_fin1 SELECT CONCAT(a.kddept, a.kdunit, a.kdprogram) as kdindex, "
            . "CONCAT(a.kddept, a.kdunit, a.kdprogram) as kdbreak, CONCAT_WS('.', a.kddept, a.kdunit, "
            . "a.kdprogram) as kode, IFNULL(nmprogram, '???') as uraian, SUM(jumlah) as jumlah, 1 as lvl "
            . "FROM z_item a "
            . "LEFT JOIN t_program b ON CONCAT(a.kddept, a.kdunit, a.kdprogram) = CONCAT(b.kddept,b.kdunit,b.kdprogram) "
            . "GROUP BY kdindex;");
    
    //table giat
    
    $this->db->query("DROP TABLE IF EXISTS tbl_fin2");
    $this->db->query("CREATE TEMPORARY TABLE tbl_fin2 SELECT CONCAT(a.kddept, a.kdunit, "
            . "a.kdprogram, a.kdgiat) as kdindex, CONCAT(a.kddept, a.kdunit, a.kdprogram) as kdbreak, "
            . "a.kdgiat as kode, IFNULL(nmgiat, '???') as uraian, SUM(jumlah) as jumlah, 2 as lvl "
            . "FROM z_item a LEFT JOIN t_giat b ON a.kdgiat = b.KDGIAT  "
            . "GROUP BY kdindex;");
    
    //table output
     $this->db->query("DROP TABLE IF EXISTS tbl_output2015");
     $this->db->query("CREATE TEMPORARY TABLE tbl_output2015 SELECT kdindex, kdbreak, kode, IFNULL(nmoutput, '???') as uraian, jumlah, 3 as lvl FROM z_item_output a LEFT JOIN tbl_output b ON field1=field2");
//    $this->db->query("DROP TABLE IF EXISTS tbl_output2015");
    
//    $this->db->query("CREATE TEMPORARY TABLE tbl_output2015 SELECT CONCAT(a.kddept,a.kdunit,a.kdprogram,a.kdgiat,a.kdoutput,a.kdlokasi,a.kdkabkota,a.kddekon,a.kdib) kdindex, 
//                CONCAT(a.kddept,a.kdunit,a.kdprogram) as kdbreak, 
//                CONCAT_WS('.', a.kdgiat,a.kdoutput) as kode, IFNULL(nmoutput, '???') as uraian, SUM(jumlah) as jumlah, 3 as lvl
//                FROM z_item a LEFT JOIN t_output b ON CONCAT(a.kdgiat,a.kdoutput)=CONCAT(b.kdgiat,b.kdoutput)
//                LEFT JOIN t_dekon c ON a.kddekon = c.kddekon 
//                LEFT JOIN t_kabkota d ON CONCAT(a.kdlokasi,a.kdkabkota)=CONCAT(d.kdlokasi,d.kdkabkota) 
//                LEFT JOIN d_output e ON CONCAT(a.thang,a.kdjendok,a.kdsatker,a.kddept,a.kdunit,a.kdprogram,a.kdgiat,a.kdoutput,a.kdlokasi,a.kdkabkota,a.kddekon,a.kdib)= 
//                CONCAT(e.thang,e.kdjendok,e.kdsatker,e.kddept,e.kdunit,e.kdprogram,e.kdgiat,e.kdoutput,e.kdlokasi,e.kdkabkota,e.kddekon,e.kdib) 
//                LEFT JOIN t_ib f ON a.kdib=f.kdib 
//                group BY kdindex; ");
   
    
    //RKAL 2016....................................................
    
     $this->db->query("DROP TABLE IF EXISTS z_item1");
    $this->db->query("CREATE TEMPORARY TABLE z_item1 SELECT THANG, KDJENDOK, KDSATKER, KDDEPT, KDUNIT, KDPROGRAM, KDGIAT, KDOUTPUT, KDLOKASI, KDKABKOTA, KDDEKON, 
    IFNULL(KDSOUTPUT, '000') as KDSOUTPUT, KDKMPNEN, KDSKMPNEN, KDAKUN, KDKPPN, KDBEBAN, KDJNSBAN, KDCTARIK, REGISTER, CARAHITUNG, HEADER1, 
    HEADER2, KDHEADER, NOITEM, NMITEM, VOL1, SAT1, VOL2, SAT2, VOL3, SAT3, VOL4, SAT4, VOLKEG, SATKEG, 
    HARGASAT, JUMLAH, JUMLAH2, PAGUPHLN, PAGURMP, PAGURKP, KDBLOKIR, BLOKIRPHLN, BLOKIRRMP, BLOKIRRKP, 
    RPHBLOKIR, KDCOPY, KDABT, KDSBU, VOLSBK, VOLRKAKL, BLNKONTRAK, NOKONTRAK, TGKONTRAK, NILKONTRAK, JANUARI, PEBRUARI, 
    MARET, APRIL, MEI, JUNI, JULI, AGUSTUS, SEPTEMBER, OKTOBER, NOPEMBER, DESEMBER, JMLTUNDA, KDLUNCURAN, JMLABT, NOREV, 
    KDUBAH, KURS, INDEXKPJM, KDIB
    FROM d_item WHERE kdsatker = 562103 AND thang=2016;");
    
     $this->db->query("DROP TABLE IF EXISTS z_item_output1");
    $this->db->query("CREATE TEMPORARY TABLE z_item_output1 SELECT CONCAT(a.kddept,a.kdunit,a.kdprogram,a.kdgiat,a.kdoutput,a.kdlokasi,a.kdkabkota,a.kddekon,a.kdib) kdindex, CONCAT(a.kddept,a.kdunit,a.kdprogram) as kdbreak, CONCAT_WS('.', a.kdgiat,a.kdoutput) as kode, SUM(jumlah) as jumlah, 3 as lvl, CONCAT(a.kdgiat,a.kdoutput) as field1 FROM z_item1 a GROUP BY kdindex;");
    
    $this->db->query("DROP TABLE IF EXISTS tbl_output1");
    $this->db->query("CREATE TEMPORARY TABLE tbl_output1 SELECT *, CONCAT(kdgiat, kdoutput) as field2 from t_output");

    //RKAKL  tblprogram
    
    $this->db->query("DROP TABLE IF EXISTS tbl_fin11");
    
    $this->db->query("CREATE TEMPORARY TABLE tbl_fin11 SELECT CONCAT(a.kddept, a.kdunit, a.kdprogram) as kdindex, "
            . "CONCAT(a.kddept, a.kdunit, a.kdprogram) as kdbreak, CONCAT_WS('.', a.kddept, a.kdunit, "
            . "a.kdprogram) as kode, IFNULL(nmprogram, '???') as uraian, SUM(jumlah) as jumlah, 1 as lvl "
            . "FROM z_item1 a "
            . "LEFT JOIN t_program b ON CONCAT(a.kddept, a.kdunit, a.kdprogram) = CONCAT(b.kddept,b.kdunit,b.kdprogram) "
            . "GROUP BY kdindex;");
    
    //table giat
    
    $this->db->query("DROP TABLE IF EXISTS tbl_fin21");
    
    $this->db->query("CREATE TEMPORARY TABLE tbl_fin21 SELECT CONCAT(a.kddept, a.kdunit, "
            . "a.kdprogram, a.kdgiat) as kdindex, CONCAT(a.kddept, a.kdunit, a.kdprogram) as kdbreak, "
            . "a.kdgiat as kode, IFNULL(nmgiat, '???') as uraian, SUM(jumlah) as jumlah, 2 as lvl "
            . "FROM z_item1 a LEFT JOIN t_giat b ON a.kdgiat = b.KDGIAT "
            . "GROUP BY kdindex;");
    
    //table output
     $this->db->query("DROP TABLE IF EXISTS tbl_output2016");
     $this->db->query("CREATE TEMPORARY TABLE tbl_output2016 SELECT kdindex, kdbreak, kode, IFNULL(nmoutput, '???') as uraian, jumlah, 3 as lvl FROM z_item_output1 a LEFT JOIN tbl_output1 b ON field1=field2");
//    $this->db->query("CREATE TEMPORARY TABLE tbl_output2016 SELECT CONCAT(a.kddept,a.kdunit,a.kdprogram,a.kdgiat,a.kdoutput,a.kdlokasi,a.kdkabkota,a.kddekon,a.kdib) kdindex, 
//                CONCAT(a.kddept,a.kdunit,a.kdprogram) as kdbreak, 
//                CONCAT_WS('.', a.kdgiat,a.kdoutput) as kode, IFNULL(nmoutput, '???') as uraian, SUM(jumlah) as jumlah, 3 as lvl
//                FROM z_item1 a LEFT JOIN t_output b ON CONCAT(a.kdgiat,a.kdoutput)=CONCAT(b.kdgiat,b.kdoutput)
//                LEFT JOIN t_dekon c ON a.kddekon = c.kddekon 
//                LEFT JOIN t_kabkota d ON CONCAT(a.kdlokasi,a.kdkabkota)=CONCAT(d.kdlokasi,d.kdkabkota) 
//                LEFT JOIN d_output e ON CONCAT(a.thang,a.kdjendok,a.kdsatker,a.kddept,a.kdunit,a.kdprogram,a.kdgiat,a.kdoutput,a.kdlokasi,a.kdkabkota,a.kddekon,a.kdib)= 
//                CONCAT(e.thang,e.kdjendok,e.kdsatker,e.kddept,e.kdunit,e.kdprogram,e.kdgiat,e.kdoutput,e.kdlokasi,e.kdkabkota,e.kddekon,e.kdib) 
//                LEFT JOIN t_ib f ON a.kdib=f.kdib 
//                group BY kdindex; ");
    
    //Kalau Satu Tahun Pakai Ini
    //$qry = "SELECT * FROM tbl_fin11 UNION SELECT * FROM tbl_fin21 order by kdindex";
        
    $this->db->query("DROP TABLE IF EXISTS tbl_rka2015");
    $this->db->query("CREATE TEMPORARY TABLE tbl_rka2015 SELECT * FROM tbl_fin1 " 
            . "UNION SELECT * FROM tbl_fin2 "
            . "UNION SELECT * FROM tbl_output2015 " 
            . "order by kdindex");
    
    $this->db->query("DROP TABLE IF EXISTS tbl_rka2016");
    $this->db->query("CREATE TEMPORARY TABLE tbl_rka2016 SELECT * FROM tbl_fin11 "
            . "UNION SELECT * FROM tbl_fin21 "
            . "UNION SELECT * FROM tbl_output2016 " 
            . "order by kdindex");
    
    $this->db->query("DROP TABLE IF EXISTS tabkey");
    $this->db->query("CREATE TEMPORARY TABLE tabkey SELECT tbl_rka2015.kdindex as kdindex, tbl_rka2015.kode as kode, tbl_rka2015.lvl as lvl "
            . "FROM tbl_rka2015 UNION SELECT tbl_rka2016.kdindex as kdindex, tbl_rka2016.kode as kode, tbl_rka2016.lvl as lvl "
            . "FROM tbl_rka2016 order by kdindex");
    
    $qry = "SELECT tabkey.kdindex as kdindex, tabkey.kode as kode, tabkey.lvl as lvl, "
            . "tbl_rka2015.kdbreak as kdbreak2015, tbl_rka2016.kdbreak as kdbreak2016, "
            . "tbl_rka2015.uraian as uraian2015, tbl_rka2016.uraian as uraian2016, tbl_rka2015.jumlah as jumlah2015, "
            . "tbl_rka2016.jumlah as jumlah2016 " 
            . "FROM tabkey "
            . "LEFT JOIN "
            . "tbl_rka2015 on tabkey.kdindex = tbl_rka2015.kdindex "
            . "LEFT JOIN "
            . "tbl_rka2016 on tabkey.kdindex = tbl_rka2016.kdindex order by kdindex";
    
            
    //Kalau Satu Tahun Pakai ini        
    //$qry = "SELECT * FROM tbl_fin1 UNION SELECT * FROM tbl_fin2 order by kdindex";

    //$this->db->select("a.kdsatker, nmsatker,a.kddept,a.kdunit, a.kddekon, nmdekon2 nmdekon, a.thang FROM tmp_satk a LEFT JOIN t_satker b ON a.kdsatker = b.kdsatker LEFT JOIN t_dekon c ON a.kddekon= c.kddekon"); 
    //$this->db->from('tmp_satk a');
    //$this->db->join('left', 'a.kdsatker=b.kdsatker');
    $query = $this->db->query($qry);
    return $query->result();
 }
 
 function getDataRKAKL2016(){
    $this->db->query("DROP TABLE IF EXISTS z_item1");
    $this->db->query("CREATE TEMPORARY TABLE z_item1 SELECT THANG, KDJENDOK, KDSATKER, KDDEPT, KDUNIT, KDPROGRAM, KDGIAT, KDOUTPUT, KDLOKASI, KDKABKOTA, KDDEKON, 
    IFNULL(KDSOUTPUT, '000'), KDKMPNEN, KDSKMPNEN, KDAKUN, KDKPPN, KDBEBAN, KDJNSBAN, KDCTARIK, REGISTER, CARAHITUNG, HEADER1, 
    HEADER2, KDHEADER, NOITEM, NMITEM, VOL1, SAT1, VOL2, SAT2, VOL3, SAT3, VOL4, SAT4, VOLKEG, SATKEG, 
    HARGASAT, JUMLAH, JUMLAH2, PAGUPHLN, PAGURMP, PAGURKP, KDBLOKIR, BLOKIRPHLN, BLOKIRRMP, BLOKIRRKP, 
    RPHBLOKIR, KDCOPY, KDABT, KDSBU, VOLSBK, VOLRKAKL, BLNKONTRAK, NOKONTRAK, TGKONTRAK, NILKONTRAK, JANUARI, PEBRUARI, 
    MARET, APRIL, MEI, JUNI, JULI, AGUSTUS, SEPTEMBER, OKTOBER, NOPEMBER, DESEMBER, JMLTUNDA, KDLUNCURAN, JMLABT, NOREV, 
    KDUBAH, KURS, INDEXKPJM, KDIB
    FROM d_item WHERE kdsatker = 562103 AND thang=2016;");

    $this->db->query("DROP TABLE IF EXISTS c_akun1");
    
    $this->db->query("CREATE TEMPORARY TABLE c_akun1 SELECT CARAHITUNG, KDAKUN, KDBEBAN, KDBLOKIR, KDCTARIK, KDDEKON, KDDEPT, KDGIAT,
    KDIB, KDJENDOK, KDJNSBAN, KDKABKOTA, KDKMPNEN, KDKPPN, KDLOKASI, KDLUNCURAN, 
    KDOUTPUT, KDPROGRAM, KDSATKER, KDSKMPNEN, IFNULL(KDSOUTPUT, '000') AS KDSOUTPUT, KDUNIT, KPPNPHLN, KPPNRKP,
    KPPNRMP, PROSENPHLN, PROSENRKP, PROSENRMP, REGDAM, REGISTER, THANG, URAIBLOKIR
    FROM d_akun WHERE kdsatker = 562103;");
    
    $this->db->query("DROP TABLE IF EXISTS tbl_fin11");
    
    $this->db->query("CREATE TEMPORARY TABLE tbl_fin11 SELECT CONCAT(a.kddept, a.kdunit, a.kdprogram) as kdindex, "
            . "CONCAT(a.kddept, a.kdunit, a.kdprogram) as kdbreak, CONCAT_WS('.', a.kddept, a.kdunit, "
            . "a.kdprogram) as kode, IFNULL(nmprogram, '???') as uraian, SUM(jumlah) as jumlah, 1 as lvl "
            . "FROM z_item1 a "
            . "LEFT JOIN t_program b ON CONCAT(a.kddept, a.kdunit, a.kdprogram) = CONCAT(b.kddept,b.kdunit,b.kdprogram) "
            . "GROUP BY kdindex;");
    
    
    
    $this->db->query("DROP TABLE IF EXISTS tbl_fin21");
    
    $this->db->query("CREATE TEMPORARY TABLE tbl_fin21 SELECT CONCAT(a.kddept, a.kdunit, "
            . "a.kdprogram, a.kdgiat) as kdindex, CONCAT(a.kddept, a.kdunit, a.kdprogram) as kdbreak, "
            . "a.kdgiat as kode, IFNULL(nmgiat, '???') as uraian, SUM(jumlah) as jumlah, 2 as lvl "
            . "FROM z_item1 a LEFT JOIN t_giat b ON CONCAT(a.kddept, a.kdgiat) = CONCAT(a.kddept, b.kdgiat) "
            . "GROUP BY kdindex;");
    
    $qry = "SELECT * FROM tbl_fin11 UNION SELECT * FROM tbl_fin21 order by kdindex";
    
    $query = $this->db->query($qry);
    return $query->result();
    
 }
}