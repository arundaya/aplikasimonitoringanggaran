<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class home_app_model extends CI_Model {
        
        public function getLoginData($usr, $psw, $kdsatker, $redi)
        {
            $u = mysql_real_escape_string($usr);
            $p = md5(mysql_real_escape_string($psw));
            $s = mysql_real_escape_string($kdsatker);
            
            $this->db->select('tblSatker.kdsatker, tblSatker.nmsatker, tblSatker.kddept, tblSatker.kddekon, tblSatker.nmdekon, tbluser.id, tbluser.UserName, tbluser.password, tbluser.Nama, tbluser.Status, tbluser.created_at, tbluser.updated_at');
            $this->db->from('tblSatker');
            $this->db->join('tbluser', 'tblSatker.kdsatker = tbluser.kdsatker');
            $this->db->where(array('UserName' => $u, 'password' => $p, 'tblSatker.kdsatker' => $s));
            $q_cek_login = $this->db->get();
            
            if(count($q_cek_login->result())>0)
            {
                foreach($q_cek_login->result() as $qck)
                {
                    if($qck->Status=='satker')
                    {
                        $this->db->select('tblSatker.kdsatker, tblSatker.nmsatker, tblSatker.kddept, tblSatker.kddekon, tblSatker.nmdekon, tbluser.id, tbluser.UserName, tbluser.password, tbluser.Nama, tbluser.Status, tbluser.created_at, tbluser.updated_at, tbluser.image');
                        $this->db->from('tblSatker');
                        $this->db->join('tbluser', 'tblSatker.kdsatker = tbluser.kdsatker');
                        $this->db->where(array('UserName' => $u, 'tblSatker.kdsatker' => $s));
                        $q_ambil_data = $this->db->get();
                        foreach ($q_ambil_data -> result() as $qad)
                        {
                            $sess_data['logged_in']         = 'yes';
                            $sess_data['Satker']        = $qad -> nmsatker;
                            $sess_data['stts']        = $qad -> Status;
                            $sess_data['nama']        = $qad -> Nama;
                            $sess_data['UserName']        = $qad -> UserName;
                            $sess_data['image']        = $qad -> image;
                          $this->session->set_userdata($sess_data);
                        }
                    if(isset($this->session->userdata['redirect_to']))
                    {
                        header('location:'.$this->session->userdata['redirect_to']);
                    }
                    else
                    {
                    header('location:'.base_url());
                    }
                    }
                    else if ($qck->Status=='admin')
                    {
                        $this->db->select('tblSatker.kdsatker, tblSatker.nmsatker, tblSatker.kddept, tblSatker.kddekon, tblSatker.nmdekon, tbluser.id, tbluser.UserName, tbluser.password, tbluser.Nama, tbluser.Status, tbluser.created_at, tbluser.updated_at, tbluser.image');
                        $this->db->from('tblSatker');
                        $this->db->join('tbluser', 'tblSatker.kdsatker = tbluser.kdsatker');
                        $this->db->where(array('UserName' => $u, 'tblSatker.kdsatker' => $s));
                        $q_ambil_data = $this->db->get();
                        foreach ($q_ambil_data -> result() as $qad)
                        {
                            $sess_data['logged_in']         = 'yes';
                            $sess_data['Satker']        = $qad -> nmsatker;
                            $sess_data['stts']        = $qad -> Status;
                            $sess_data['nama']        = $qad -> Nama;
                            $sess_data['UserName']        = $qad -> UserName;
                            $sess_data['image']        = $qad -> image;
                            $this->session->set_userdata($sess_data);
                        }
                    if(isset($this->session->userdata['redirect_to']))
                    {
                        header('location:'.$this->session->userdata['redirect_to']);
                    }
                    else
                    {
                    header('location:'.base_url());
                    }
                    }  
                }
            }
            else
            {
                $this->session->set_flashdata('pesan_error', 'Username atau Password salah');
                header('location:'.base_url());
            }
        }
        
        function getDataSatker(){
                $this->db->select("nmsatker, kdsatker FROM `tblSatker"); 
                $query = $this->db->get();
                return $query->result();    
         }


         
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */