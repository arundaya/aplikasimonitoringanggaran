<?php
class SettingModel extends CI_Model
{

    function getDataSatker()
        {
                    $this->db->select("nmsatker, kdsatker FROM `tblSatker"); 
                    $query = $this->db->get();
                    return $query->result();    
             }
             
    function getDataUser()
        {
                    $this->db->select('tblSatker.kdsatker, tblSatker.nmsatker, tblSatker.kddept, tblSatker.kddekon, tblSatker.nmdekon, tbluser.id, tbluser.UserName, tbluser.password, tbluser.Nama, tbluser.Status, tbluser.created_at, tbluser.updated_at');
                    $this->db->from('tblSatker');
                    $this->db->join('tbluser', 'tblSatker.kdsatker = tbluser.kdsatker');
                    $query = $this->db->get();
                    return $query->result();    
             }

}
