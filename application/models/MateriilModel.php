<?php
class MateriilModel extends CI_Model
{
    
    public function __construct() {
        parent::__construct();
    }
    
function json() {
    
$this->datatables->select('tbl_materiil.kode as ID, tbl_materiil.uraian as nmsatker, tbl_materiil.uraian as UserName');
$this->datatables->from('tbl_materiil');
$this->datatables->add_column('view', '<a href="world/edit/$1">edit</a> | <a href="world/delete/$1">delete</a>', 'kdindex');
return $this->datatables->generate();
}

function json2() {
$this->datatables->select('tbl_materiil.kode as kode, tbl_materiil.uraian as uraian, tbl_materiil.satuan as satuan, tbl_materiil.merek as merek');
$this->datatables->from('tbl_materiil');
return $this->datatables->generate();
}

function getDataMateriil(){
    
    $qry = "SELECT tabkey.kdindex as kdindex, tabkey.kode as kode, tabkey.lvl as lvl, "
            . "tbl_materiil.kdbreak as kdbreak,  "
            . "tbl_materiil.uraian as uraian FROM tabkey "
            . "LEFT JOIN "
            . "tbl_materiil on tabkey.kdindex = tbl_materiil.kdindex  order by kdindex";
     
    $query = $this->db->query($qry);
    return $query->result();
 }
 
 function getSaldoAwal(){
    
    $qry = "SELECT tabkey.kdindex as kdindex, tabkey.kode as kode, tabkey.lvl as lvl, "
            . "tbl_materiil.kdbreak as kdbreak,  "
            . "tbl_materiil.uraian as uraian FROM tabkey "
            . "LEFT JOIN "
            . "tbl_materiil on tabkey.kdindex = tbl_materiil.kdindex  order by kdindex limit 30";
     
    $query = $this->db->query($qry);
    return $query->result();
 }
 
 function getDataHibah(){
   
    $qry = "SELECT tabkey.kdindex as kdindex, tabkey.kode as kode, tabkey.lvl as lvl, "
            . "tbl_materiil.kdbreak as kdbreak,  "
            . "tbl_materiil.uraian as uraian FROM tabkey "
            . "LEFT JOIN "
            . "tbl_materiil on tabkey.kdindex = tbl_materiil.kdindex  order by kdindex limit 30";
     
    $query = $this->db->query($qry);
    return $query->result();
 }
 
 function getPenguranganMateriil(){
    
    $qry = "SELECT tabkey.kdindex as kdindex, tabkey.kode as kode, tabkey.lvl as lvl, "
            . "tbl_materiil.kdbreak as kdbreak,  "
            . "tbl_materiil.uraian as uraian FROM tabkey "
            . "LEFT JOIN "
            . "tbl_materiil on tabkey.kdindex = tbl_materiil.kdindex  order by kdindex  limit 30";
     
    $query = $this->db->query($qry);
    return $query->result();
 }
 
 function getPerubahanMateriil(){
    
    $qry = "SELECT tabkey.kdindex as kdindex, tabkey.kode as kode, tabkey.lvl as lvl, "
            . "tbl_materiil.kdbreak as kdbreak,  "
            . "tbl_materiil.uraian as uraian FROM tabkey "
            . "LEFT JOIN "
            . "tbl_materiil on tabkey.kdindex = tbl_materiil.kdindex  order by kdindex  limit 30";
     
    $query = $this->db->query($qry);
    return $query->result();
 }
}
