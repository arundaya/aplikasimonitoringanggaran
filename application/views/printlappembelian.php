<style type="text/css">
        h1 {text-align:center; font-size:18px;}
        h2 {font-size:14px;}
        .tengah {text-align:center;	}
        .kiri {padding-left:5px;}
        
        table.nilai {border-collapse: collapse;}
        table.nilai td {border: 1px solid #000000}
</style>
<style>
        TR.double {border-top: double;}
</style>
    
<style type="text/css">
<!--
    table.page_header {width: 100%; border: none; background-color: #DDDDFF; border-bottom: solid 1mm #AAAADD; padding: 5mm }
    table.page_footer {width: 100%; border: none; padding-left: 5mm; padding-top:5mm}

    
-->
</style>

<page backtop="25mm" backbottom="5mm" backleft="5mm" backright="8mm" style="font-size: 9pt">
  <page_header>
      <p text-align="center">LAPORAN PEMBELIAN MATERIIL<br>    
      SEMESTER ..1..<br>
      TAHUN ANGGARAN ..2016..</p>
        <table class="page_footer" width="500px">
             <tr>
                <td style="width: 100px; font-size: 9pt">
                    SATUAN KERJA
                </td>
                <td style="width: 30px; font-size: 9pt">
                    :
                </td>
                <td style="width: 50px; font-size: 9pt">
                    (562103)
                </td>
                <td style="width: 250px; font-size: 9pt">
                    KEMENTERIAN PERTAHANAN
                </td>
            </tr>
        </table>
      <br/>
    </page_header> 
    <page_footer>
        
    </page_footer>
    <br/>
    <table width="100%" class="nilai">
        <thead>
            
            <tr>
                <td colspan="6" style="text-align:right;border-top:none;border-bottom:none;border-left:none;border-right:none"></td>
                <td style="border-top:none;border-bottom:none;border-left:none;border-right:none">Tanggal : 11 November 2016</td>
                </tr>
                <tr>
                    <td colspan="6" style="text-align:right;border-top:none;border-bottom:none;border-left:none;border-right:none"></td>
                <td  style="border-top:none;border-bottom:none;border-left:none;border-right:none">Halaman [[page_cu]]/[[page_nb]]</td>
            </tr>
            <tr>
                <td colspan="7" style="text-align:right;border-top:none;border-left:none;border-right:none"></td>
            </tr>
            
            <tr  class="double">
                <td width="70" class="tengah">TANGGAL BELI</td>
                <td width="70"  class="tengah">KODE</td>

                <td width="300" class="tengah">URAIAN</td>

                <td  width="70" class="tengah">SATUAN</td>

                <td width="70" class="tengah">MEREK</td>
                <td  width="120" class="tengah">LOKASI</td>
                <td width="70" class="tengah">JUMLAH</td>
            </tr>
            
        </thead>
        
										
										<tbody>
											<?php
                          foreach($materiil as $materiil)
                                
                                 { ?>
                                  
											<tr>
                                                <td></td>
												<td><?php echo $materiil->kode; ?></td>
                                                <td><?php echo $materiil->uraian; ?></td>
												<td></td>
												<td></td>
                                                <td></td>
                                                <td></td>
                                                </tr>
										<?php	}
                      
                      ?>
											<tr>
                    <td colspan="6" style="text-align:right;border-top:none;border-bottom:none;border-left:none;border-right:none"></td>
                <td  style="border-top:none;border-bottom:none;border-left:none;border-right:none">Tanda Tangan</td>
            </tr>
										</tbody>
</table>
</page>

