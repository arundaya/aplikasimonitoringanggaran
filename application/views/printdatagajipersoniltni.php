<style type="text/css">
        h1 {text-align:center; font-size:18px;}
        h2 {font-size:14px;}
        .tengah {text-align:center;	}
        .kiri {padding-left:5px;}
        
        
       
        table.nilai {border-collapse: collapse;font-family:arial;font-size:11.2}
        table.nilai td {border: 1px solid #000000}
        
        
</style>
<style>
        TR.double {border-top: double;}
</style>
    
<style type="text/css">
<!--
    table.page_header {width: 100%; border: none; background-color: #DDDDFF; border-bottom: solid 1mm #AAAADD; padding: 5mm }
    table.page_footer {width: 100%; border: none; padding-left: 5mm; padding-top:5mm}

    
-->
</style>

<page backtop="22mm" backbottom="5mm" backleft="5mm" backright="8mm" style="font-size: 9pt">
  <page_header>
      <p text-align="center">DAFTAR PEMBAYARAN PENGHASILAN DAN ULP PRAJURIT TNI</p>
        <table class="page_footer" width="500px">
            <tr>
                <td style="width: 100px; font-size: 9pt">
                    UNIT KERJA
                </td>
                <td style="width: 30px; font-size: 9pt">
                    :
                </td>
                <td style="width: 50px; font-size: 9pt">
                    (562103)
                </td>
                <td style="width: 250px; font-size: 9pt">
                    KEMENTERIAN PERTAHANAN
                </td>
            </tr>
            <tr>
                <td style="width: 100px; font-size: 9pt">
                    SATKER
                </td>
                <td style="width: 30px; font-size: 9pt">
                    :
                </td>
                <td style="width: 50px; font-size: 9pt">
                    
                </td>
                <td style="width: 250px; font-size: 9pt">
                    ROREN
                </td>
            </tr>
        </table>
      <br/>
    </page_header> 
    <page_footer>
        
    </page_footer>
    
    <table width="100%" class="nilai">
        <thead>
            
            <tr>
                <td colspan="14" style="text-align:right;border-top:none;border-left:none;border-right:none;border-bottom:none;">Status : Warakawuri</td>
            </tr>
            <tr>
                <td colspan="14" style="text-align:right;border-top:none;border-left:none;border-right:none;">Halaman [[page_cu]]/[[page_nb]]</td>
            </tr>
            
            <tr>
                <td rowspan="2" class="kiri">NO. <br/> URUT</td>
                <td rowspan="2" class="kiri"><br/>NAMA <br/>
                    NRP <br/>
                    TGL. LAHIR - TMP JBT<br/>
                    PANGKAT<br/>
                    JABATAN<br/>
                    NPWP
                </td>
                <td rowspan="2" class="kiri">
                    JAB/ESLN <br/>
                    STS KAWIN <br/>
                    TMT KGB <br/>
                    MKG
                </td>
                <td colspan="6" class="tengah">P E N G H A S I L A N</td>
                
                
                
                
                
                <td colspan="4" class="tengah">P O T O N G A N</td>
                
                
                <td rowspan="2" class="kiri" >
                    JUMLAH <br/>
                    BERSIH GAJI <br/>
                    LAUK PAUK <br/>
                    ----- <br/>
                    JUMLAH <br/>
                    DIBAYARKAN
                </td>
            </tr>
            <tr>
                
                <td class="kiri">G. POKOK <br/>
                    T. ISTRI/SUAMI <br/>
                    T. ANAK <br/>
                    G. BRUTO
                </td>
                <td class="kiri">T. STRUKTURAL <br/>
                    T. FUNGSIONAL <br/>
                    T. UMUM
                </td>
                <td class="kiri">T. PGN BERAS <br/>
                    T. KOWAN <br/>
                    T. SANDI/KOMPEN <br/>
                    T. BABINSA
                </td>
                <td class="kiri">T. PAPUA <br/>
                    T. PENCIL <br/>
                    T. P. TERLUAR <br/>
                    T. TERAMPIL
                </td>
                <td class="kiri">T. LAINNYA <br/>
                    TPP <br/>
                    T. PPh <br/>
                    PEMBULATAN 
                </td>
                <td class="kiri">JUMLAH <br/>
                    PENGHASILAN <br/>
                    KOTOR 
                </td>
                <td class="kiri">P. BERAS <br/>
                    P. PENSIUN <br/>
                    P. BPJS <br/>
                    P. THT
                </td>
                <td class="kiri">SEWA RUMAH <br/>
                    UTANG <br/>
                    PPh Ps 21
                </td>
                <td class="kiri">PENGEMBALIAN <br/>
                    TGR <br/>
                    POT LAINNYA 
                </td>
                <td class="kiri">JUMLAH <br/>
                    POTONGAN
                </td>
                
            </tr>
            <tr>
                <td class="tengah">1</td>
                <td class="tengah">2</td>
                <td class="tengah">3</td>
                <td class="tengah">4</td>
                <td class="tengah">5</td>
                <td class="tengah">6</td>
                <td class="tengah">7</td>
                <td class="tengah">8</td>
                <td class="tengah">9</td>
                <td class="tengah">10</td>
                <td class="tengah">11</td>
                <td class="tengah">12</td>
                <td class="tengah">13</td>
                <td class="tengah">14</td>
            </tr>
        </thead>
        <tbody style="font-family:arial;font-size:9">
            <tr>
                <td class="kiri">1.</td>
                <td class="kiri">DODIK KUSYANTO <br/>
                    14276/P P <br/>
                    02-05-1977 &nbsp;&nbsp;&nbsp; 01-01-2015 <br/>
                    MAYORPALAKSA <br/>
                    000000000043000
                </td>
                <td class="tengah">Prajurit TNI <br/>
                    1102 <br/>
                    01-12-2011 <br/>
                    4A14
                </td>
                <td class="tengah">4</td>
                <td class="tengah">5</td>
                <td class="tengah">6</td>
                <td class="tengah">7</td>
                <td class="tengah">8</td>
                <td class="tengah">9</td>
                <td class="tengah">10</td>
                <td class="tengah">11</td>
                <td class="tengah">12</td>
                <td class="tengah">13</td>
                <td class="tengah">14</td>
            </tr>
    </tbody>
</table>
</page>

