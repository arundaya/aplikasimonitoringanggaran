<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="description" content="A fully featured admin theme which can be used to build CRM, CMS, etc.">
		<meta name="author" content="Coderthemes">

		<link rel="shortcut icon" href="<?php echo base_url('assets/images/favicon_1.ico'); ?>">

		<title>Aplikasi Monitoring Anggaran</title>
		<link href="<?php echo base_url('assets/plugins/bootstrap-tagsinput/css/bootstrap-tagsinput.css'); ?>" rel="stylesheet" />
		 <link href="<?php echo base_url('assets/plugins/bootstrap-table/css/bootstrap-table.min.css'); ?>" rel="stylesheet" type="text/css" />
                <link href="<?php echo base_url('assets/plugins/switchery/css/switchery.min.css'); ?>" rel="stylesheet" />
                <link href="<?php echo base_url('assets/plugins/multiselect/css/multi-select.css'); ?>"  rel="stylesheet" type="text/css" />
                <link href="<?php echo base_url('assets/plugins/select2/css/select2.min.css'); ?>" rel="stylesheet" type="text/css" />
                <link href="<?php echo base_url('assets/plugins/bootstrap-select/css/bootstrap-select.min.css'); ?>" rel="stylesheet" />
                <link href="<?php echo base_url('assets/plugins/bootstrap-touchspin/css/jquery.bootstrap-touchspin.min.css'); ?>" rel="stylesheet" />
                 
                <link href="<?php echo base_url('assets/css/bootstrap.min.css'); ?>" rel="stylesheet" type="text/css" />
                <link href="<?php echo base_url('assets/css/core.css'); ?>" rel="stylesheet" type="text/css" />
                <link href="<?php echo base_url('assets/css/components.css'); ?>" rel="stylesheet" type="text/css" />
                <link href="<?php echo base_url('assets/css/icons.css'); ?>" rel="stylesheet" type="text/css" />
                <link href="<?php echo base_url('assets/css/pages.css'); ?>" rel="stylesheet" type="text/css" />
                <link href="<?php echo base_url('assets/css/responsive.css'); ?>" rel="stylesheet" type="text/css" />

        <!-- HTML5 Shiv and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->

        <script src="<?php echo base_url('assets/js/modernizr.min.js'); ?>"></script>

	</head>

	<body class="fixed-left">

		<!-- Begin page -->
		<div id="wrapper">

            <!-- Top Bar Start -->
            <?php
                $this->load->view('topbar');
             ?>
            <!-- Top Bar End -->


            <!-- ========== Left Sidebar Start ========== -->
            <?php
                $this->load->view('leftmenu');
             ?>
			<!-- Left Sidebar End -->

			<!-- ============================================================== -->
			<!-- Start right Content here -->
			<!-- ============================================================== -->
			<div class="content-page">
				<!-- Start content -->
				<div class="content">
                                    
					<div class="container">
                                            <div class="row">
							<div class="col-sm-12">
                                <?php
                                if($this->session->userdata['stts']=='admin')
                                    {
                                    ?>
                        <div class="btn-group pull-right m-t-15">
                                    <button type="button" class="btn btn-default dropdown-toggle waves-effect waves-light" data-toggle="dropdown" aria-expanded="false">Settings <span class="m-l-5"><i class="fa fa-cog"></i></span></button>
                                    <ul class="dropdown-menu drop-menu-right" role="menu">
                                        <li><a href="#">Daftar Satker</a></li>
                                        <li><a href="#">Daftar User</a></li>
                                        
                                    </ul>
                                </div>
                        
                        <?php
                                    
                                    }
                                    ?>

								<h4 class="page-title">Home</h4>
								<ol class="breadcrumb">
									<li class="active">
										Profil User
									</li>
									
								</ol>
							</div>
						</div>

                        <!--Basic Columns-->
						<!--===================================================-->
						
						<div class="row">
							<div class="col-md-4 col-lg-4">
                                <div class="profile-detail card-box">
                                    <div>
                                        
                                        
                                            
                                       
                                        
                                        <img src="
                                            
                                       <?php 
                                       if(empty($this->session->userdata['image'] ))
                                        {   
                                           echo base_url('assets/images/users/no-image.jpg'); ?>" class="img-circle" alt="profile-image">
                                        <?php 
                                        } 
                                        else
                                        {
                                            echo base_url($this->session->userdata['image']); ?>" class="img-circle" alt="profile-image">
                                        <?php
                                        }
                                        ?>
                                      
                                            

                                        <hr>
                                        

                                        <div class="text-left">
                                            <table>
                                                <tr>
                                                    <td>
                                                        <p class="text-muted font-13"><strong>Nama Lengkap &nbsp;&nbsp;</strong></p>
                                                    </td>
                                                    <td>
                                                        <p class="text-muted font-13">:</p>
                                                    </td>
                                                    <td>
                                                        <p><span class="m-l-15"><?php echo $this->session->userdata['nama']; ?></span></p>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <p class="text-muted font-13"><strong>Satker</strong></p>
                                                    </td>
                                                    <td>
                                                        <p class="text-muted font-13">:</p>
                                                    </td>
                                                    <td>
                                                        <p><span class="m-l-15"><?php echo $this->session->userdata['Satker']; ?></span></p>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <p class="text-muted font-13"><strong>User Name</strong></p>
                                                    </td>
                                                    <td>
                                                        <p class="text-muted font-13">:</p>
                                                    </td>
                                                    <td>
                                                        <p><span class="m-l-15"><?php echo $this->session->userdata['UserName']; ?></span></p>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>


                                        
                                    </div>

                                </div>

                            </div>
                                                    
                                                    
							<div class="col-lg-6">
								<div class="card-box">
									<h4 class="m-t-0 header-title"><b>Ganti Password</b></h4>
									<p class="text-muted font-13 m-b-30">
	                                    
	                                </p>
		                                        
									<form action="#" data-parsley-validate novalidate>
										
										<div class="form-group">
											<label for="pass1">Password*</label>
											<input id="pass1" type="password" placeholder="Password" required class="form-control">
										</div>
										<div class="form-group">
											<label for="passWord2">Confirm Password *</label>
											<input data-parsley-equalto="#pass1" type="password" required placeholder="Password" class="form-control" id="passWord2">
										</div>
										<div class="form-group">
														<label class="control-label">Ganti Photo</label>
														<input type="file" class="filestyle" data-buttonname="btn-white">
													</div>

										<div class="form-group text-right m-b-0">
											<button class="btn btn-primary waves-effect waves-light" type="submit">
												Submit
											</button>
											<button type="reset" class="btn btn-default waves-effect waves-light m-l-5">
												Cancel
											</button>
										</div>
                                                                            
										
									</form>
								</div>
							</div>
						</div>
						
						

                    </div> <!-- container -->
                               
                </div> <!-- content -->

                <footer class="footer">
                    © 2016. All rights reserved.
                </footer>

            </div>
            <!-- ============================================================== -->
            <!-- End Right content here -->
            <!-- ============================================================== -->


            <!-- Right Sidebar -->
            <div class="side-bar right-bar nicescroll">
                <h4 class="text-center">Chat</h4>
                <div class="contact-list nicescroll">
                    <ul class="list-group contacts-list">
                        <li class="list-group-item">
                            <a href="#">
                                <div class="avatar">
                                    <img src="assets/images/users/avatar-1.jpg" alt="">
                                </div>
                                <span class="name">Chadengle</span>
                                <i class="fa fa-circle online"></i>
                            </a>
                            <span class="clearfix"></span>
                        </li>
                        <li class="list-group-item">
                            <a href="#">
                                <div class="avatar">
                                    <img src="assets/images/users/avatar-2.jpg" alt="">
                                </div>
                                <span class="name">Tomaslau</span>
                                <i class="fa fa-circle online"></i>
                            </a>
                            <span class="clearfix"></span>
                        </li>
                        <li class="list-group-item">
                            <a href="#">
                                <div class="avatar">
                                    <img src="assets/images/users/avatar-3.jpg" alt="">
                                </div>
                                <span class="name">Stillnotdavid</span>
                                <i class="fa fa-circle online"></i>
                            </a>
                            <span class="clearfix"></span>
                        </li>
                        <li class="list-group-item">
                            <a href="#">
                                <div class="avatar">
                                    <img src="assets/images/users/avatar-4.jpg" alt="">
                                </div>
                                <span class="name">Kurafire</span>
                                <i class="fa fa-circle online"></i>
                            </a>
                            <span class="clearfix"></span>
                        </li>
                        <li class="list-group-item">
                            <a href="#">
                                <div class="avatar">
                                    <img src="assets/images/users/avatar-5.jpg" alt="">
                                </div>
                                <span class="name">Shahedk</span>
                                <i class="fa fa-circle away"></i>
                            </a>
                            <span class="clearfix"></span>
                        </li>
                        <li class="list-group-item">
                            <a href="#">
                                <div class="avatar">
                                    <img src="assets/images/users/avatar-6.jpg" alt="">
                                </div>
                                <span class="name">Adhamdannaway</span>
                                <i class="fa fa-circle away"></i>
                            </a>
                            <span class="clearfix"></span>
                        </li>
                        <li class="list-group-item">
                            <a href="#">
                                <div class="avatar">
                                    <img src="assets/images/users/avatar-7.jpg" alt="">
                                </div>
                                <span class="name">Ok</span>
                                <i class="fa fa-circle away"></i>
                            </a>
                            <span class="clearfix"></span>
                        </li>
                        <li class="list-group-item">
                            <a href="#">
                                <div class="avatar">
                                    <img src="assets/images/users/avatar-8.jpg" alt="">
                                </div>
                                <span class="name">Arashasghari</span>
                                <i class="fa fa-circle offline"></i>
                            </a>
                            <span class="clearfix"></span>
                        </li>
                        <li class="list-group-item">
                            <a href="#">
                                <div class="avatar">
                                    <img src="assets/images/users/avatar-9.jpg" alt="">
                                </div>
                                <span class="name">Joshaustin</span>
                                <i class="fa fa-circle offline"></i>
                            </a>
                            <span class="clearfix"></span>
                        </li>
                        <li class="list-group-item">
                            <a href="#">
                                <div class="avatar">
                                    <img src="assets/images/users/avatar-10.jpg" alt="">
                                </div>
                                <span class="name">Sortino</span>
                                <i class="fa fa-circle offline"></i>
                            </a>
                            <span class="clearfix"></span>
                        </li>
                    </ul>  
                </div>
            </div>
            <!-- /Right-bar -->


        </div>
        <!-- END wrapper -->
    
        <script>
            var resizefunc = [];
        </script>

        <!-- jQuery  -->
        <script src="<?php echo base_url('assets/js/jquery.min.js'); ?>"></script>
        <script src="<?php echo base_url('assets/js/bootstrap.min.js'); ?>"></script>
        <script src="<?php echo base_url('assets/js/detect.js'); ?>"></script>
        <script src="<?php echo base_url('assets/js/fastclick.js'); ?>"></script>
        <script src="<?php echo base_url('assets/js/jquery.slimscroll.js'); ?>"></script>
        <script src="<?php echo base_url('assets/js/jquery.blockUI.js'); ?>"></script>
        <script src="<?php echo base_url('assets/js/waves.js'); ?>"></script>
        <script src="<?php echo base_url('assets/js/wow.min.js'); ?>"></script>
        <script src="<?php echo base_url('assets/js/jquery.nicescroll.js'); ?>"></script>
        <script src="<?php echo base_url('assets/js/jquery.scrollTo.min.js'); ?>"></script>
        
        <script src="<?php echo base_url('assets/plugins/bootstrap-tagsinput/js/bootstrap-tagsinput.min.js'); ?>"></script>
        <script src="<?php echo base_url('assets/plugins/switchery/js/switchery.min.js'); ?>"></script>
        <script type="text/javascript" src="<?php echo base_url('assets/plugins/multiselect/js/jquery.multi-select.js'); ?>"></script>
        <script type="text/javascript" src="<?php echo base_url('assets/plugins/jquery-quicksearch/jquery.quicksearch.js'); ?>"></script>
        <script src="<?php echo base_url('assets/plugins/select2/js/select2.min.js'); ?>" type="text/javascript"></script>
        <script src="<?php echo base_url('assets/plugins/bootstrap-select/js/bootstrap-select.min.js'); ?>" type="text/javascript"></script>
        <script src="<?php echo base_url('assets/plugins/bootstrap-filestyle/js/bootstrap-filestyle.min.js'); ?>" type="text/javascript"></script>
        <script src="<?php echo base_url('assets/plugins/bootstrap-touchspin/js/jquery.bootstrap-touchspin.min.js'); ?>" type="text/javascript"></script>
        <script src="<?php echo base_url('assets/plugins/bootstrap-maxlength/bootstrap-maxlength.min.js'); ?>" type="text/javascript"></script>

        <script type="text/javascript" src="<?php echo base_url('assets/plugins/autocomplete/jquery.mockjax.js'); ?>"></script>
        <script type="text/javascript" src="<?php echo base_url('assets/plugins/autocomplete/jquery.autocomplete.min.js'); ?>"></script>
        <script type="text/javascript" src="<?php echo base_url('assets/plugins/autocomplete/countries.js'); ?>"></script>
        <script type="text/javascript" src="<?php echo base_url('assets/pages/autocomplete.js'); ?>"></script>

        <script type="text/javascript" src="<?php echo base_url('assets/pages/jquery.form-advanced.init.js'); ?>"></script>

        <script src="<?php echo base_url('assets/js/jquery.core.js'); ?>"></script>
        <script src="<?php echo base_url('assets/js/jquery.app.js'); ?>"></script>

        <script src="<?php echo base_url('assets/plugins/bootstrap-table/js/bootstrap-table.min.js'); ?>"></script>

        <script src="<?php echo base_url('assets/pages/jquery.bs-table.js'); ?>"></script>


	
	</body>
</html>