<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="description" content="A fully featured admin theme which can be used to build CRM, CMS, etc.">
		<meta name="author" content="Coderthemes">

		<link rel="shortcut icon" href="<?php echo base_url('assets/images/favicon_1.ico'); ?>">

		<title>Aplikasi Monitoring Anggaran</title>
                <link href="<?php echo base_url('assets/plugins/datatables/jquery.dataTables.min.css'); ?>" rel="stylesheet" type="text/css"/>
		<link href="<?php echo base_url('assets/plugins/bootstrap-tagsinput/css/bootstrap-tagsinput.css'); ?>" rel="stylesheet" />
		 <link href="<?php echo base_url('assets/plugins/bootstrap-table/css/bootstrap-table.min.css'); ?>" rel="stylesheet" type="text/css" />
                <link href="<?php echo base_url('assets/plugins/switchery/css/switchery.min.css'); ?>" rel="stylesheet" />
                <link href="<?php echo base_url('assets/plugins/multiselect/css/multi-select.css'); ?>"  rel="stylesheet" type="text/css" />
                <link href="<?php echo base_url('assets/plugins/select2/css/select2.min.css'); ?>" rel="stylesheet" type="text/css" />
                <link href="<?php echo base_url('assets/plugins/bootstrap-select/css/bootstrap-select.min.css'); ?>" rel="stylesheet" />
                <link href="<?php echo base_url('assets/plugins/bootstrap-touchspin/css/jquery.bootstrap-touchspin.min.css'); ?>" rel="stylesheet" />
                 
                <link href="<?php echo base_url('assets/css/bootstrap.min.css'); ?>" rel="stylesheet" type="text/css" />
                <link href="<?php echo base_url('assets/css/core.css'); ?>" rel="stylesheet" type="text/css" />
                <link href="<?php echo base_url('assets/css/components.css'); ?>" rel="stylesheet" type="text/css" />
                <link href="<?php echo base_url('assets/css/icons.css'); ?>" rel="stylesheet" type="text/css" />
                <link href="<?php echo base_url('assets/css/pages.css'); ?>" rel="stylesheet" type="text/css" />
                <link href="<?php echo base_url('assets/css/responsive.css'); ?>" rel="stylesheet" type="text/css" />

        <!-- HTML5 Shiv and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->

        <script src="<?php echo base_url('assets/js/modernizr.min.js'); ?>"></script>

	</head>

	<body class="fixed-left">

		<!-- Begin page -->
		<div id="wrapper">

            <!-- Top Bar Start -->
            <?php
                $this->load->view('topbar');
             ?>
            <!-- Top Bar End -->

 
            <!-- ========== Left Sidebar Start ========== -->
             <?php
                $this->load->view('leftmenu');
             ?>
			<!-- Left Sidebar End -->

			<!-- ============================================================== -->
			<!-- Start right Content here -->
			<!-- ============================================================== -->
			<div class="content-page">
				<!-- Start content -->
				<div class="content">
                                    
					<div class="container">
                                            <div class="row">
							<div class="col-sm-12">
                                <?php
                                $this->load->view('adminsetting');
                                    ?>

								<h4 class="page-title">Materiil</h4>
								<ol class="breadcrumb">
									<li class="active">
										Transaksi
									</li>
									<li class="active">
										Tambah Saldo Awal Materiil
									</li>
								</ol>
							</div>
						</div>

                        <!--Basic Columns-->
						<!--===================================================-->
						
						<div class="row">
							<div class="col-sm-12">
                                                           <div class="card-box">
                                                               
                                                               
                                                               <div class="col-md-6">
                                                                   
                                                               <form class="form-horizontal" role="form">                                    
	                                            
	                                            <div class="form-group">
							<label for="userName">Satker</label>
											<input type="text" readonly="" name="nick" parsley-trigger="change" value="<?php echo $this->session->userdata['Satker']; ?>" class="form-control" id="userName">
										</div>
                                                                   
                                                    
                                                                </form>
                                                                   
                                                          
                                                               </div>
                                                               
                                                               
                                                               <br/>
                                                               <br/>
                                                               <br/>
                                                        
                                                <br/>
                                                    <div class="row"> 
                                                        <div class="col-md-6"> 
                                                            <div class="form-group"> 
                                                                <label for="field-1" class="control-label" >Kode</label> 
                                                                <div class="input-group">
                                                                    <input type="text" class="form-control" id="kode" placeholder="Kode Materiil"> 
                                                                    <div class="input-group-btn">
                                                                    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="modal" data-target="#con-close-modal">
									<span class="caret"></span>
                                                                    </button>
                                                                    </div>
								</div>
								
                                                            </div> 
                                                        </div> 
                                                    </div> 
                                                    <div class="row"> 
                                                        <div class="col-md-12"> 
                                                            <div class="form-group"> 
                                                                <label for="field-3" class="control-label">Uraian</label> 
                                                                <input type="text" class="form-control" id="uraian" placeholder="Uraian Materiil"> 
                                                            </div> 
                                                        </div> 
                                                    </div> 
                                                    <div class="row"> 
                                                        <div class="col-md-4"> 
                                                            <div class="form-group"> 
                                                                <label for="field-4" class="control-label">Satuan</label> 
                                                                <input type="text" class="form-control" id="satuan" placeholder="Unit Satuan Materiil"> 
                                                            </div> 
                                                        </div> 
                                                        <div class="col-md-4"> 
                                                            <div class="form-group"> 
                                                                <label for="field-5" class="control-label">Merek</label> 
                                                                <input type="text" class="form-control" id="merek" placeholder="Merek Jika Ada"> 
                                                            </div> 
                                                        </div>
                                                        <div class="col-md-4"> 
                                                            <div class="form-group"> 
                                                                <label for="field-5" class="control-label">Lokasi</label> 
                                                                <input type="text" class="form-control" id="field-5" placeholder="Lokasi"> 
                                                            </div> 
                                                        </div>
                                                    </div> 
                                                    <div class="row"> 
                                                        <div class="col-md-4"> 
                                                            <div class="form-group"> 
                                                                <label for="field-5" class="control-label">Kondisi</label> 
                                                                <input type="text" class="form-control" id="field-5" placeholder="Kondisi"> 
                                                            </div> 
                                                        </div>
                                                        <div class="col-md-4"> 
                                                            <div class="form-group"> 
                                                                <label for="field-5" class="control-label">Saldo Awal</label> 
                                                                <input type="text" class="form-control" id="field-5" placeholder="Saldo Awal"> 
                                                            </div> 
                                                        </div>
                                                    </div> 
                                                
                                                <div id="con-close-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                                        <div class="modal-dialog"> 
                                            <div class="modal-content"> 
                                                <div class="modal-header"> 
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button> 
                                                    <h4 class="modal-title">Data Materiil</h4> 
                                                </div> 
                                                <div class="modal-body"> 
                                        <table id="datatable-fixed-col" class="table table-striped table-bordered">
                                            <thead>
											<tr>
												<th data-field="kode" data-switchable="false">KODE</th>
												<th data-field="uraian">URAIAN</th>
                                                <th data-field="satuan">SATUAN</th>
                                                <th data-field="merek">MEREK</th>
                                            </tr>
										</thead>
										
										
									</table>
                                                </div> 
                                                <div class="modal-footer"> 
                                                    <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Batal</button> 
                                                    <button type="button" class="btn btn-info waves-effect waves-light">Simpan</button> 
                                                </div> 
                                            </div> 
                                        </div>
                                    </div>
                                               
								</div>
							</div>
						</div>
						
						

                    </div> <!-- container -->
                               
                </div> <!-- content -->

                <footer class="footer">
                    © 2016. All rights reserved.
                </footer>

            </div>
            <!-- ============================================================== -->
            <!-- End Right content here -->
            <!-- ============================================================== -->


            <!-- Right Sidebar -->
            
            <!-- /Right-bar -->


        </div>
        <!-- END wrapper -->
    
        <script>
            var resizefunc = [];
        </script>

        <!-- jQuery  -->
        <script src="<?php echo base_url('assets/js/jquery.min.js'); ?>"></script>
        <script src="<?php echo base_url('assets/js/bootstrap.min.js'); ?>"></script>
        <script src="<?php echo base_url('assets/js/detect.js'); ?>"></script>
        <script src="<?php echo base_url('assets/js/fastclick.js'); ?>"></script>
        <script src="<?php echo base_url('assets/js/jquery.slimscroll.js'); ?>"></script>
        <script src="<?php echo base_url('assets/js/jquery.blockUI.js'); ?>"></script>
        <script src="<?php echo base_url('assets/js/waves.js'); ?>"></script>
        <script src="<?php echo base_url('assets/js/wow.min.js'); ?>"></script>
        <script src="<?php echo base_url('assets/js/jquery.nicescroll.js'); ?>"></script>
        <script src="<?php echo base_url('assets/js/jquery.scrollTo.min.js'); ?>"></script>

        <script src="<?php echo base_url('assets/plugins/bootstrap-table/js/bootstrap-table.min.js'); ?>"></script>

        <script src="<?php echo base_url('assets/pages/jquery.bs-table.js'); ?>"></script>


        <script src="<?php echo base_url('assets/js/jquery.core.js'); ?>"></script>
        <script src="<?php echo base_url('assets/js/jquery.app.js'); ?>"></script>
        
        
        <script src="<?php echo base_url('assets/datatables/jquery.dataTables.js') ?>" ></script>
        <script src="<?php echo base_url('assets/datatables/dataTables.bootstrap.js') ?>"></script>
        <script type="text/javascript">
            $(document).ready(function() {
                $.fn.dataTableExt.oApi.fnPagingInfo = function(oSettings)
                {
                    return {
                        "iStart": oSettings._iDisplayStart,
                        "iEnd": oSettings.fnDisplayEnd(),
                        "iLength": oSettings._iDisplayLength,
                        "iTotal": oSettings.fnRecordsTotal(),
                        "iFilteredTotal": oSettings.fnRecordsDisplay(),
                        "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
                        "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
                    };
                };

                var t = $("#datatable-fixed-col").dataTable({
                    initComplete: function() {
                        var api = this.api();
                        $('#mytable_filter input')
                                .off('.DT')
                                .on('keyup.DT', function(e) {
                                    if (e.keyCode == 13) {
                                        api.search(this.value).draw();
                            }
                        });
                    },
                    oLanguage: {
                        sProcessing: "loading..."
                    },
                    scrollY: "300px",
                    scrollX: true,
                    scrollCollapse: true,
                    paging: false,
                    processing: true,
                    serverSide: true,
                    ajax: {"url": "json2", "type": "POST"},
                    columns: [
                        {
                            "data": "kode",
                            "orderable": false
                        },
                        {"data": "uraian"},
                        {"data": "satuan"},
                        {"data": "merek"}
                    ],
                    order: [[1, 'asc']],
                    rowCallback: function(row, data, iDisplayIndex) {
                        var info = this.fnPagingInfo();
                        var page = info.iPage;
                        var length = info.iLength;
                        var index = page * length + (iDisplayIndex + 1);
                        $('td:eq(0)', row).html(index);
                    }
                });
            });
        </script>
        <script type="text/javascript"> 

$(document).ready(function ()  
{  
  
$('#datatable-fixed-col tbody').on( 'click', 'tr', function () {  
  
//Prevent the hyperlink to perform default behavior  
event.preventDefault();  
//alert($(event.target).text())  
    var $td= $(this).closest('tr').children('td');  
  
  
var kode= $td.eq(0).text();  
var uraian= $td.eq(1).text();
var satuan= $td.eq(2).text();  
var merek= $td.eq(3).text();

document.getElementById("kode").value = kode;
document.getElementById("uraian").value = uraian;
    document.getElementById("satuan").value = satuan;
document.getElementById("merek").value = merek;
 
     $('#con-close-modal').modal('hide');}  
  
);  
  
});  
  
</script>

	
	</body>
</html>