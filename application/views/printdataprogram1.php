<style type="text/css">
        h1 {text-align:center; font-size:18px;}
        h2 {font-size:14px;}
        .tengah {text-align:center;	}
        .kiri {padding-left:5px;}
        
        table.nilai {border-collapse: collapse;}
        table.nilai td {border: 1px solid #000000}
</style>
<style>
        TR.double {border-top: double;}
</style>
    
<style type="text/css">
<!--
    table.page_header {width: 100%; border: none; background-color: #DDDDFF; border-bottom: solid 1mm #AAAADD; padding: 5mm }
    table.page_footer {width: 100%; border: none; padding-left: 5mm; padding-top:5mm}

    
-->
</style>

<page backtop="25mm" backbottom="5mm" backleft="5mm" backright="8mm" style="font-size: 9pt">
  <page_header>
      <p text-align>RINCIAN KERTAS KERJA T.A. 2015, 2015 REV V, 2016 REV VI</p>
        <table class="page_footer" width="500px">
            <tr>
                <td style="width: 100px; font-size: 9pt">
                    KEMEN / LEMB
                </td>
                <td style="width: 30px; font-size: 9pt">
                    :
                </td>
                <td style="width: 50px; font-size: 9pt">
                    (012)
                </td>
                <td style="width: 250px; font-size: 9pt">
                    KEMENTERIAN PERTAHANAN
                </td>
            </tr>
            <tr>
                <td style="width: 100px; font-size: 9pt">
                    UNIT ORG
                </td>
                <td style="width: 30px; font-size: 9pt">
                    :
                </td>
                <td style="width: 50px; font-size: 9pt">
                    (01)
                </td>
                <td style="width: 250px; font-size: 9pt">
                    KEMENTERIAN PERTAHANAN
                </td>
            </tr>
             <tr>
                <td style="width: 100px; font-size: 9pt">
                    UNIT KERJA
                </td>
                <td style="width: 30px; font-size: 9pt">
                    :
                </td>
                <td style="width: 50px; font-size: 9pt">
                    (562103)
                </td>
                <td style="width: 250px; font-size: 9pt">
                    KEMENTERIAN PERTAHANAN
                </td>
            </tr>
        </table>
      
    </page_header> 
    <page_footer>
        
    </page_footer>
    <br/>
    <table width="100%" class="nilai">
        <thead>
            
            <tr>
                <td colspan="7" style="text-align:right;border-top:none;border-left:none;border-right:none">Halaman [[page_cu]]/[[page_nb]]</td>
            </tr>
            
            <tr  class="double">
                <td width="70" rowspan="2" class="tengah">KODE</td>

                <td colspan="2" class="tengah">DATA RKAKL T.A. 2015</td>

                <td colspan="2" class="tengah">DATA RKAKL T.A. 2016 REV V</td>

                <td colspan="2" class="tengah">DATA RKAKL T.A. 2016 REV VI</td>
            </tr>
            <tr>
                <td width="190" class="tengah">PROGRAM/KEGIATAN/OUTPUT</td>

                <td width="100" class="tengah">JUMLAH BIAYA</td>

                 <td width="190" class="tengah">PROGRAM/KEGIATAN/OUTPUT</td>

                <td width="100" class="tengah">JUMLAH BIAYA</td>

                <td width="190" class="tengah">PROGRAM/KEGIATAN/OUTPUT</td>

                <td width="100" class="tengah">JUMLAH BIAYA</td>
            </tr>
        </thead>
        <tbody>
    <?php
     
                          foreach($rkakl as $rkakl)
     
                          { 
                              ?>
    
            
            <?php 
                                    $leve = $rkakl->lvl;
                                    if($leve==1)
                                        {
                                          echo "<tr style='font-weight:bold'>";
                                          echo "<td class='kiri' valign='center'>";
                                          echo "$rkakl->kode";
                                        } 
                                        elseif ($leve=2) 
                                            {
                                            echo "<tr>";
                                            echo "<td class='kiri'>";
                                            echo "$rkakl->kode";
                                            }
                                            elseif ($leve=3) 
                                                {
                                                    echo "<tr>";
                                                    echo "<td class='kiri'>";
                                                    echo "$rkakl->kode";
                                                }
                                                else 
                                                    {
                                                    echo "<tr>";
                                                    echo "<td class='kiri'>";
                                                    echo "$rkakl->kode";
                                                    }
        
         ?></td>
        
        <td width="190" class="kiri"><?php echo "$rkakl->uraian2015" ?></td>
        <td style="text-align: right;padding-right: 5"><?php echo number_format($rkakl->jumlah2015) ?></td>
        <td width="190" class="kiri"><?php echo "$rkakl->uraian2016" ?></td>
        <td style="text-align: right;padding-right: 5"><?php echo number_format($rkakl->jumlah2016) ?></td>
        <td width="190" class="kiri"><?php echo "$rkakl->uraian2016" ?></td>
        <td style="text-align: right;padding-right: 5"><?php echo number_format($rkakl->jumlah2016) ?></td>
        
    </tr>
    <?php
    
                          } 
                          ?>
    </tbody>
</table>
</page>

