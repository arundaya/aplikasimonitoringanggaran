<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="description" content="A fully featured admin theme which can be used to build CRM, CMS, etc.">
		<meta name="author" content="Coderthemes">

		<link rel="shortcut icon" href="<?php echo base_url('assets/images/favicon_1.ico'); ?>">

		<title>Aplikasi Monitoring Anggaran</title>
		<link href="<?php echo base_url('assets/plugins/bootstrap-tagsinput/css/bootstrap-tagsinput.css'); ?>" rel="stylesheet" />
		 <link href="<?php echo base_url('assets/plugins/bootstrap-table/css/bootstrap-table.min.css'); ?>" rel="stylesheet" type="text/css" />
                <link href="<?php echo base_url('assets/plugins/switchery/css/switchery.min.css'); ?>" rel="stylesheet" />
                <link href="<?php echo base_url('assets/plugins/multiselect/css/multi-select.css'); ?>"  rel="stylesheet" type="text/css" />
                <link href="<?php echo base_url('assets/plugins/select2/css/select2.min.css'); ?>" rel="stylesheet" type="text/css" />
                <link href="<?php echo base_url('assets/plugins/bootstrap-select/css/bootstrap-select.min.css'); ?>" rel="stylesheet" />
                <link href="<?php echo base_url('assets/plugins/bootstrap-touchspin/css/jquery.bootstrap-touchspin.min.css'); ?>" rel="stylesheet" />
                 
                <link href="<?php echo base_url('assets/css/bootstrap.min.css'); ?>" rel="stylesheet" type="text/css" />
                <link href="<?php echo base_url('assets/css/core.css'); ?>" rel="stylesheet" type="text/css" />
                <link href="<?php echo base_url('assets/css/components.css'); ?>" rel="stylesheet" type="text/css" />
                <link href="<?php echo base_url('assets/css/icons.css'); ?>" rel="stylesheet" type="text/css" />
                <link href="<?php echo base_url('assets/css/pages.css'); ?>" rel="stylesheet" type="text/css" />
                <link href="<?php echo base_url('assets/css/responsive.css'); ?>" rel="stylesheet" type="text/css" />

        <!-- HTML5 Shiv and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->

        <script src="<?php echo base_url('assets/js/modernizr.min.js'); ?>"></script>

	</head>

	<body class="fixed-left">

		<!-- Begin page -->
		<div id="wrapper">

            <!-- Top Bar Start -->
             <?php
                $this->load->view('topbar');
             ?>
            <!-- Top Bar End -->


            <!-- ========== Left Sidebar Start ========== -->
<?php
                $this->load->view('leftmenu');
             ?>
			<!-- Left Sidebar End -->

			<!-- ============================================================== -->
			<!-- Start right Content here -->
			<!-- ============================================================== -->
			<div class="content-page">
				<!-- Start content -->
				<div class="content">
					<div class="container">
<!-- Start content -->
				<div class="content">
					<div class="container">
                                            <div class="row">
							<div class="col-sm-12">
                                <?php
                                $this->load->view('adminsetting');
                                    ?>

								<h4 class="page-title">Materiil</h4>
								<ol class="breadcrumb">
									<li class="active">
										Transaksi
									</li>
									<li class="active">
										Data Hibah
									</li>
								</ol>
							</div>
						</div>
                        <!--Basic Columns-->
						<!--===================================================-->
						
						<div class="row">
							<div class="col-sm-12">
								<div class="card-box">
							<br/>
                                                                                        <button class="btn btn-primary waves-effect waves-light" data-toggle="modal" data-target="#con-close-modal">TAMBAH DATA</button>
                                                                                        <button id="demo-print-row" class="btn btn-default" ><a href="printtranhibah"><i class="fa fa-print m-r-5"></i>Cetak</a></button>
                                                                                        
									 <table id="demo-custom-toolbar"  data-toggle="table"
                                               data-toolbar="#demo-delete-row"
                                               data-search="true"
                                               data-show-refresh="true"
                                               data-show-toggle="true"
                                               data-sort-name="id"
                                               data-page-list="[5, 10, 20]"
                                               data-page-size="5"
                                            data-pagination="true" data-show-pagination-switch="true" class="table-bordered ">
                                            <thead>
											<tr>
                                                    <th data-field="nomor" data-switchable="false" class="text-center">NO <br/></th>
                                                    <th data-field="kode" data-switchable="false">KODE</th>
                                                    <th data-field="uraian">NAMA MATERIIL</th>
                                                    <th data-field="kuantitas">KUANTITAS</th>
                                                    <th data-field="noawal">NO <br> AWAL</th>
                                                    <th data-field="noakhir">NO <br> AKHIR</th>
                                                    <th data-field="kondisi">KONDISI</th>
                                                    <th data-field="keterangan">KETERANGAN</th>
                                                    <th data-field="#" class="text-center">ACTION</th>
						</tr>
										</thead>
										
										<tbody>
											
                                                <tr>
                                                    <td>A01342</td>
                                                    <td>3100102001</td>
                                                                                        <td>
                                                                                            P.C Unit
                                                                                        </td>
                                                                                        <td>
                                                                                            3
                                                                                        </td>
                                                                                        <td>
                                                                                            6
                                                                                        </td>
                                                                                        <td>
                                                                                            8
                                                                                        </td>
                                                                                        <td>
                                                                                            BAIK
                                                                                        </td>
                                                                                        <td class="text-left">
                                                                                            3 Hibah dalam operasional sistem monitoring
                                                                                        </td>
                                                                                         <td><a href='#' class='on-default edit-row' data-toggle='modal' data-target='#con-close-modal'><i class='fa fa-pencil'></i></a>
	                                                <a href='#' class='on-default remove-row'><i class='fa fa-trash-o'></i></a></td>
                                                                                    </tr>
                                                                                    
                                                                                </tbody>
									</table>
								</div>
							</div>
						</div>
						
						

                    </div> <!-- container -->
                               
                </div> <!-- content -->

                <footer class="footer">
                    © 2016. All rights reserved.
                </footer>

            </div>
            <!-- ============================================================== -->
            <!-- End Right content here -->
            <!-- ============================================================== -->


            <!-- Right Sidebar -->
            <div class="side-bar right-bar nicescroll">
                <h4 class="text-center">Chat</h4>
                <div class="contact-list nicescroll">
                    <ul class="list-group contacts-list">
                        <li class="list-group-item">
                            <a href="#">
                                <div class="avatar">
                                    <img src="assets/images/users/avatar-1.jpg" alt="">
                                </div>
                                <span class="name">Chadengle</span>
                                <i class="fa fa-circle online"></i>
                            </a>
                            <span class="clearfix"></span>
                        </li>
                        <li class="list-group-item">
                            <a href="#">
                                <div class="avatar">
                                    <img src="assets/images/users/avatar-2.jpg" alt="">
                                </div>
                                <span class="name">Tomaslau</span>
                                <i class="fa fa-circle online"></i>
                            </a>
                            <span class="clearfix"></span>
                        </li>
                        <li class="list-group-item">
                            <a href="#">
                                <div class="avatar">
                                    <img src="assets/images/users/avatar-3.jpg" alt="">
                                </div>
                                <span class="name">Stillnotdavid</span>
                                <i class="fa fa-circle online"></i>
                            </a>
                            <span class="clearfix"></span>
                        </li>
                        <li class="list-group-item">
                            <a href="#">
                                <div class="avatar">
                                    <img src="assets/images/users/avatar-4.jpg" alt="">
                                </div>
                                <span class="name">Kurafire</span>
                                <i class="fa fa-circle online"></i>
                            </a>
                            <span class="clearfix"></span>
                        </li>
                        <li class="list-group-item">
                            <a href="#">
                                <div class="avatar">
                                    <img src="assets/images/users/avatar-5.jpg" alt="">
                                </div>
                                <span class="name">Shahedk</span>
                                <i class="fa fa-circle away"></i>
                            </a>
                            <span class="clearfix"></span>
                        </li>
                        <li class="list-group-item">
                            <a href="#">
                                <div class="avatar">
                                    <img src="assets/images/users/avatar-6.jpg" alt="">
                                </div>
                                <span class="name">Adhamdannaway</span>
                                <i class="fa fa-circle away"></i>
                            </a>
                            <span class="clearfix"></span>
                        </li>
                        <li class="list-group-item">
                            <a href="#">
                                <div class="avatar">
                                    <img src="assets/images/users/avatar-7.jpg" alt="">
                                </div>
                                <span class="name">Ok</span>
                                <i class="fa fa-circle away"></i>
                            </a>
                            <span class="clearfix"></span>
                        </li>
                        <li class="list-group-item">
                            <a href="#">
                                <div class="avatar">
                                    <img src="assets/images/users/avatar-8.jpg" alt="">
                                </div>
                                <span class="name">Arashasghari</span>
                                <i class="fa fa-circle offline"></i>
                            </a>
                            <span class="clearfix"></span>
                        </li>
                        <li class="list-group-item">
                            <a href="#">
                                <div class="avatar">
                                    <img src="assets/images/users/avatar-9.jpg" alt="">
                                </div>
                                <span class="name">Joshaustin</span>
                                <i class="fa fa-circle offline"></i>
                            </a>
                            <span class="clearfix"></span>
                        </li>
                        <li class="list-group-item">
                            <a href="#">
                                <div class="avatar">
                                    <img src="assets/images/users/avatar-10.jpg" alt="">
                                </div>
                                <span class="name">Sortino</span>
                                <i class="fa fa-circle offline"></i>
                            </a>
                            <span class="clearfix"></span>
                        </li>
                    </ul>  
                </div>
            </div>
            <!-- /Right-bar -->


        </div>
        <!-- END wrapper -->
    
        <script>
            var resizefunc = [];
        </script>

        <!-- jQuery  -->
        <script src="<?php echo base_url('assets/js/jquery.min.js'); ?>"></script>
        <script src="<?php echo base_url('assets/js/bootstrap.min.js'); ?>"></script>
        <script src="<?php echo base_url('assets/js/detect.js'); ?>"></script>
        <script src="<?php echo base_url('assets/js/fastclick.js'); ?>"></script>
        <script src="<?php echo base_url('assets/js/jquery.slimscroll.js'); ?>"></script>
        <script src="<?php echo base_url('assets/js/jquery.blockUI.js'); ?>"></script>
        <script src="<?php echo base_url('assets/js/waves.js'); ?>"></script>
        <script src="<?php echo base_url('assets/js/wow.min.js'); ?>"></script>
        <script src="<?php echo base_url('assets/js/jquery.nicescroll.js'); ?>"></script>
        <script src="<?php echo base_url('assets/js/jquery.scrollTo.min.js'); ?>"></script>

        <script src="<?php echo base_url('assets/plugins/bootstrap-table/js/bootstrap-table.min.js'); ?>"></script>

        <script src="<?php echo base_url('assets/pages/jquery.bs-table.js'); ?>"></script>

        <script src="<?php echo base_url('assets/plugins/bootstrap-tagsinput/js/bootstrap-tagsinput.min.js'); ?>"></script> 
        <script src="<?php echo base_url('assets/js/jquery.core.js'); ?>"></script>
        <script src="<?php echo base_url('assets/js/jquery.app.js'); ?>"></script>
        

	
	</body>
</html>