<style type="text/css">
        h1 {text-align:center; font-size:18px;}
        h2 {font-size:14px;}
        .tengah {text-align:center;	}
        .kiri {padding-left:5px;}
        
        table.nilai {border-collapse: collapse;}
        table.nilai td {border: 1px solid #000000}
</style>
<style>
        TR.double {border-top: double;}
</style>
    
<style type="text/css">
<!--
    table.page_header {width: 100%; border: none; background-color: #DDDDFF; border-bottom: solid 1mm #AAAADD; padding: 5mm }
    table.page_footer {width: 100%; border: none; padding-left: 5mm; padding-top:5mm}

    
-->
</style>

<page backtop="25mm" backbottom="5mm" backleft="5mm" backright="8mm" style="font-size: 9pt">
  <page_header>
      <p text-align="center">LAPORAN MATERIIL RINCIAN PER ..KELOMPOK BARANG..<br>    
      SEMESTER ..1..<br>
      TAHUN ANGGARAN ..2016..</p>
        <table class="page_footer" width="500px">
             <tr>
                <td style="width: 100px; font-size: 9pt">
                    SATUAN KERJA
                </td>
                <td style="width: 30px; font-size: 9pt">
                    :
                </td>
                <td style="width: 50px; font-size: 9pt">
                    (562103)
                </td>
                <td style="width: 250px; font-size: 9pt">
                    KEMENTERIAN PERTAHANAN
                </td>
            </tr>
        </table>
      <br/>
    </page_header> 
    <page_footer>
        
    </page_footer>
    <br/>
    <table width="100%" class="nilai">
        <thead>
            
            <tr>
                <td colspan="6" style="text-align:right;border-top:none;border-bottom:none;border-left:none;border-right:none"></td>
                <td style="border-top:none;border-bottom:none;border-left:none;border-right:none">Tanggal : 11 November 2016</td>
                </tr>
                <tr>
                    <td colspan="6" style="text-align:right;border-top:none;border-bottom:none;border-left:none;border-right:none"></td>
                <td  style="border-top:none;border-bottom:none;border-left:none;border-right:none">Halaman [[page_cu]]/[[page_nb]]</td>
            </tr>
            <tr>
                <td colspan="7" style="text-align:right;border-top:none;border-left:none;border-right:none"></td>
            </tr>
            
            <tr  class="double">
                <td width="70" rowspan="2" class="tengah">KODE</td>

                <td width="300" rowspan="2" class="tengah">URAIAN</td>

                <td rowspan="2" class="tengah">SATUAN</td>

                <td rowspan="2" class="tengah">SALDO PER 1 JANUARI 2016</td>
                <td COLSPAN="2" class="tengah">MUTASI</td>
                <td rowspan="2" class="tengah">SALDO PER 30 JUNI 2016</td>
            </tr>
            <tr>
                
                 <td width="80" class="tengah">BERTAMBAH</td>

                <td width="80" class="tengah">BERKURANG</td>

               
            </tr>
        </thead>
        								<tbody>
                                            <?php
                          foreach($materiil as $materiil)
                                
                                 { ?>
                                  
											<tr>
												<td><?php echo $materiil->kode; ?></td>
                                                <td><?php echo $materiil->uraian; ?></td>
												<td></td>
												<td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                </tr>
										<?php	}
                      
                      ?>
											<tr>
                    <td colspan="6" style="text-align:right;border-top:none;border-bottom:none;border-left:none;border-right:none"></td>
                <td  style="border-top:none;border-bottom:none;border-left:none;border-right:none">Tanda Tangan</td>
            </tr>
										</tbody>
</table>
</page>

