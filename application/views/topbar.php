<div class="topbar">

                <!-- LOGO -->
                <div class="topbar-left">
                    <div class="text-center">
                        <a href="index.html" class="logo"><i class="icon-magnet icon-c-logo"></i><span>U.O. KEMHAN</span></a>
                        
                    </div>
                </div>

                <!-- Button mobile view to collapse sidebar menu -->
                <div class="navbar navbar-default" role="navigation">
                    <div class="container">
                        <div class="">
                            <div class="pull-left">
                                <button class="button-menu-mobile open-left waves-effect waves-light">
                                    <i class="md md-menu"></i>
                                </button>
                                <span class="clearfix"></span>
                            </div>

                            <ul class="nav navbar-nav hidden-xs">
                               
                            </ul>
                             <a href="index.html" class="logo"><i class="icon-magnet icon-c-logo"></i><span>﻿<?php echo $this->session->userdata['Satker']; ?> </span></a>
                            <ul class="nav navbar-nav navbar-right pull-right">
                                
                                <li class="hidden-xs">
                                    <a href="#" id="btn-fullscreen" class="waves-effect waves-light"><i class="icon-size-fullscreen"></i></a>
                                </li>
                               
                                
                                    
                                <li class="dropdown top-menu-item-xs">
                                    <a href="" class="dropdown-toggle profile waves-effect waves-light" data-toggle="dropdown" aria-expanded="true">
                                        
                                        <img src="<?php 
                                       if(empty($this->session->userdata['image'] ))
                                        {   
                                           echo base_url('assets/images/users/no-image.jpg'); ?>" class="img-circle" alt="profile-image">
                                        <?php 
                                        } 
                                        else
                                        {
                                            echo base_url($this->session->userdata['image']); ?>" class="img-circle" alt="profile-image">
                                        <?php
                                        }
                                        ?></a>
                                    <ul class="dropdown-menu">
                                        <li><a href="<?php echo base_url('/profile'); ?>"><i class="ti-user m-r-10 text-custom"></i> Profile</a></li>
                                        <li><a href="<?php echo base_url('/profile/gantifoto'); ?>"><i class="ti-settings m-r-10 text-custom"></i> Settings</a></li>
                                        <li class="divider"></li>
                                        <li><a href="<?php echo base_url('/home/logout'); ?>"><i class="ti-power-off m-r-10 text-danger"></i> Logout</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                        <!--/.nav-collapse -->
                    </div>
                </div>
            </div>