<div class="left side-menu">
                <div class="sidebar-inner slimscrollleft">
                    <!--- Divider -->
                    <div id="sidebar-menu">
                        <ul>

                        	<li class="text-muted menu-title">Home</li>

                            <li class="has_sub">
                                <a href="<?php echo base_url('/'); ?>" class="waves-effect"><i class="ti-home"></i> <span> Dashboard </span> </a>
                                
                            </li>

                            

                            <li class="text-muted menu-title">Materiil</li>

                            <li class="has_sub">
                                <a href="javascript:void(0);" class="waves-effect"><i class="ti-files"></i><span> Tabel Referensi </span> <span class="menu-arrow"></span></a>
                                <ul class="list-unstyled">
                                <li><a href="<?php echo base_url('/materiil/tabelmateriil'); ?>">Tabel Materiil</a></li>
                                    <li><a href="<?php echo base_url('/materiil/tabelsatuan'); ?>">Tabel Satuan</a></li>
                                    <li><a href="<?php echo base_url('/materiil/tabelmerek'); ?>">Tabel Merek</a></li>
                                    <li><a href="<?php echo base_url('/materiil/tabellokasi'); ?>">Tabel Lokasi</a></li>
                                </ul>
                            </li>

                            <li class="has_sub">
                                <a href="javascript:void(0);" class="waves-effect"><i class="ti-gift"></i><span>Transaksi Materiil </span> <span class="menu-arrow"></span></a>
                                <ul class="list-unstyled">
                                    <li><a href="<?php echo base_url('/materiil/saldoawal'); ?>">Saldo Awal</a></li>
                                    <li><a href="<?php echo base_url('/materiil/pembelian'); ?>">Pembelian</a></li>
                                    <li><a href="<?php echo base_url('/materiil/hibah'); ?>">Hibah</a></li>
                                    
                                </ul>
                            </li>

                            <li class="has_sub">
                                <a href="javascript:void(0);" class="waves-effect"><i class="ti-email"></i><span> Perubahan Materiil </span> <span class="menu-arrow"></span></a>
                                <ul class="list-unstyled">
                                    <li><a href="<?php echo base_url('/materiil/pengurangan'); ?>"> Pengurangan</a></li>
                                    <li><a href="<?php echo base_url('/materiil/perubahankondisi'); ?>"> Perubahan Kondisi</a></li>
                                </ul>
                            </li>

                            <li class="has_sub">
                                <a href="javascript:void(0);" class="waves-effect"><i class="ti-share"></i><span>Laporan </span> <span class="menu-arrow"></span></a>
                                
                                <ul>
                                    <li><a href="<?php echo base_url('/materiil/laporandatamateriil'); ?>"> Data Materiil</a></li>
                                    <li class="has_sub">
                                        <a href="javascript:void(0);" class="waves-effect"><span>Kondisi Materiil</span>  <span class="menu-arrow"></span></a>
                                        <ul style="">
                                            <li><a href="javascript:void(0);"><span>Baik</span></a></li>
                                            <li><a href="javascript:void(0);"><span>Rusak Ringan</span></a></li>
                                            <li><a href="javascript:void(0);"><span>Rusak Berat</span></a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </li>
                            
                            <li class="text-muted menu-title">Sismet</li>

                            <li class="has_sub">
                                <a href="javascript:void(0);" class="waves-effect"><i class="ti-files"></i><span> Tabel Referensi </span> <span class="menu-arrow"></span></a>
                                <ul class="list-unstyled">
                                	<li><a href="<?php echo base_url('/sismet/tabelrefjnssismet'); ?>">Jenis Sismet</a></li>
                                </ul>
                                
                                <a href="javascript:void(0);" class="waves-effect"><i class="ti-files"></i><span> Data Sismet </span> <span class="menu-arrow"></span></a>
                                <ul class="list-unstyled">
                                	 <li><a href="<?php echo base_url('/sismet'); ?>">  Data Sismet</a></li>
                                </ul>
                            </li>

                            <li class="text-muted menu-title">Personil</li>

                            <li class="has_sub">
                                <a href="javascript:void(0);" class="waves-effect"><i class="ti-user"></i><span> Personil TNI  </span> <span class="menu-arrow"></span></a>
                                <ul class="list-unstyled">
                                    <li><a href="<?php echo base_url('/Personil'); ?>">Data Personil TNI</a></li>
                                    <li><a href="crm-contact.html"> Upload Data Personil </a></li>
                                    <li><a href="crm-opportunities.html"> Data Pegawai Non Aktif </a></li>
                                </ul>
                            </li>
                            
                            <li class="has_sub">
                                <a href="javascript:void(0);" class="waves-effect"><i class="ti-user"></i><span> Personil PNS  </span> <span class="menu-arrow"></span></a>
                                <ul class="list-unstyled">
                                    <li><a href="crm-dashboard.html"> Data Personil PNS </a></li>
                                    <li><a href="crm-contact.html"> Upload Data Personil </a></li>
                                    <li><a href="crm-opportunities.html"> Data Pegawai Non Aktif </a></li>
                                </ul>
                            </li>

                             <li class="has_sub">
                                <a href="javascript:void(0);" class="waves-effect"><i class="ti-user"></i><span> Laporan  </span> <span class="menu-arrow"></span></a>
                                <ul class="list-unstyled">
                                    <li><a href="<?php echo base_url('/Personil/viewlaporangajitni'); ?>">  Daftar Gaji TNI </a></li>
                                    <li><a href="<?php echo base_url('/Personil/viewlaporangajitni'); ?>">  Daftar Gaji PNS </a></li>
                                    <li><a href="crm-opportunities.html">  Data TNI Pindah </a></li>
                                    <li><a href="crm-opportunities.html">  Data PNS Pindah </a></li>
                                </ul>
                            </li>
                            
                            <li class="text-muted menu-title">Monitoring Rengrar</li>

                            <li class="has_sub">
                                <a href="<?php echo base_url('/dataprogram'); ?>" class="waves-effect"><i class="ti-files"></i><span>Perbandingan RKAKL </span> </a>
                                <a href="javascript:void(0);" class="waves-effect"><i class="ti-files"></i><span>Upload Data </span> </a>
                                <a href="javascript:void(0);" class="waves-effect"><i class="ti-files"></i><span>Laporan Rengar </span> </a>
                            </li>


                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
