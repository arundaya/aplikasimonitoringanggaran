<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="description" content="A fully featured admin theme which can be used to build CRM, CMS, etc.">
		<meta name="author" content="Coderthemes">

		<link rel="shortcut icon" href="<?php echo base_url('assets/images/favicon_1.ico'); ?>">

		<title>Aplikasi Monitoring Anggaran</title>
		<link href="<?php echo base_url('assets/plugins/bootstrap-tagsinput/css/bootstrap-tagsinput.css'); ?>" rel="stylesheet" />
		 <link href="<?php echo base_url('assets/plugins/bootstrap-table/css/bootstrap-table.min.css'); ?>" rel="stylesheet" type="text/css" />
                <link href="<?php echo base_url('assets/plugins/switchery/css/switchery.min.css'); ?>" rel="stylesheet" />
                <link href="<?php echo base_url('assets/plugins/multiselect/css/multi-select.css'); ?>"  rel="stylesheet" type="text/css" />
                <link href="<?php echo base_url('assets/plugins/select2/css/select2.min.css'); ?>" rel="stylesheet" type="text/css" />
                <link href="<?php echo base_url('assets/plugins/bootstrap-select/css/bootstrap-select.min.css'); ?>" rel="stylesheet" />
                <link href="<?php echo base_url('assets/plugins/bootstrap-touchspin/css/jquery.bootstrap-touchspin.min.css'); ?>" rel="stylesheet" />
                 
                <link href="<?php echo base_url('assets/css/bootstrap.min.css'); ?>" rel="stylesheet" type="text/css" />
                <link href="<?php echo base_url('assets/css/core.css'); ?>" rel="stylesheet" type="text/css" />
                <link href="<?php echo base_url('assets/css/components.css'); ?>" rel="stylesheet" type="text/css" />
                <link href="<?php echo base_url('assets/css/icons.css'); ?>" rel="stylesheet" type="text/css" />
                <link href="<?php echo base_url('assets/css/pages.css'); ?>" rel="stylesheet" type="text/css" />
                <link href="<?php echo base_url('assets/css/responsive.css'); ?>" rel="stylesheet" type="text/css" />

        <!-- HTML5 Shiv and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->

        <script src="<?php echo base_url('assets/js/modernizr.min.js'); ?>"></script>

	</head>

	<body class="fixed-left">

		<!-- Begin page -->
		<div id="wrapper">

            <!-- Top Bar Start -->
            <?php
                $this->load->view('topbar');
             ?>
            <!-- Top Bar End -->


            <!-- ========== Left Sidebar Start ========== -->

            <?php
                $this->load->view('leftmenu');
             ?>
			<!-- Left Sidebar End -->

			<!-- ============================================================== -->
			<!-- Start right Content here -->
			<!-- ============================================================== -->
			<div class="content-page">
				<!-- Start content -->
				<div class="content">
					<div class="container">

                        <!--Basic Columns-->
						<!--===================================================-->
						
						<div class="row">
							<div class="col-sm-12">
								<div class="card-box">
									<h4 class="m-t-0 header-title"><b>TABEL PERUBAHAN KONDISI</b></h4>
									<p class="text-muted font-13">
										DATA KONDISI MATERIIL DAN FASILITAS BANGUNAN
									</p>
                                    <form class="form-horizontal" role="form">                                    
	                                            <div class="form-group">
	                                                <label class="col-md-2 control-label">UNIT KERJA</label>
	                                                <div class="col-md-10">
	                                                    <select class="form-control select2">
		                                        <option>Select</option>
		                                        
		                                            <option value="AK">KEMENTERIAN PERTAHANAN</option>
		                                            
		                                        
		                                    </select>
	                                                </div>
                                                        
                                                        <label class="col-md-2 control-label">SEMESTER</label>
		                                        <div class="radio radio-info radio-inline">
		                                            <input type="radio" id="inlineRadio1" value="option1" name="radioInline" checked>
		                                            <label for="inlineRadio1"> SEMESTER SATU </label>
		                                        </div>
		                                        <div class="radio radio-inline">
		                                            <input type="radio" id="inlineRadio2" value="option2" name="radioInline">
		                                            <label for="inlineRadio2"> SEMESTER DUA </label>
		                                        </div>
	                                            </div>
	                                        </form>
									<table data-toggle="table"
										   data-show-columns="false"
										   data-page-list="[5, 10, 20]"
										   data-page-size="5"
										   data-pagination="true" data-show-pagination-switch="true" class="table-bordered ">
										<thead>
											<tr>
												<th data-field="kode" data-switchable="false">TANGGAL PERUBAHAN</th>
                                                <th data-field="kode" data-switchable="false">KODE</th>
												<th data-field="uraian">URAIAN</th>
                                                <th data-field="satuan">SATUAN</th>
                                                <th data-field="satuan">MEREK</th>
                                                <th data-field="satuan">LOKASI</th>
                                                <th data-field="satuan">KONDISI</th>
                                                <th data-field="satuan">SALDO</th>
												<th data-field="#" class="text-center">ACTION</th>
											</tr>
										</thead>
										
										<tbody>
											<?php
                          foreach($materiil as $materiil)
                                
                                 {
                                  echo "<td>";
                                  echo "</td>";
                                  echo "<td>";
                                  echo "$materiil->kode";
                                  echo "</td>";
                                  echo "<td>";
                                  echo "$materiil->uraian";
                                  //echo "$rkakl->uraian";
                                  echo "</td>";
                                  echo "<td>";
                                  echo "</td>";
                                  echo "<td>";
                                  echo "</td>";
                                  echo "<td>";
                                  echo "</td>";
                                  echo "<td>";
                                  echo "</td>";
                                  echo "<td>";
                                  echo "</td>";
                                 
                                  echo"
                                  <td><a href='#' class='on-default edit-row' data-toggle='modal' data-target='#con-close-modal'><i class='fa fa-pencil'></i></a>
	                                                <a href='#' class='on-default remove-row'><i class='fa fa-trash-o'></i></a></td>";
                                  echo "</tr>";
                                  }
                      
                      ?>
										</tbody>
									</table>
                                    <div id="con-close-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                                        <div class="modal-dialog"> 
                                            <div class="modal-content"> 
                                                <div class="modal-header"> 
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button> 
                                                    <h4 class="modal-title">Perubahan Kondisi Materiil</h4> 
                                                </div> 
                                                <div class="modal-body"> 
                                                    <div class="row"> 
                                                        <div class="col-md-6"> 
                                                            <div class="form-group"> 
                                                                <label for="field-1" class="control-label">Kode</label> 
                                                                <input type="text" class="form-control" id="field-1" placeholder="Kode Materiil"> 
                                                            </div> 
                                                        </div> 
                                                    </div> 
                                                    <div class="row"> 
                                                        <div class="col-md-12"> 
                                                            <div class="form-group"> 
                                                                <label for="field-3" class="control-label">Uraian</label> 
                                                                <input type="text" class="form-control" id="field-3" placeholder="Uraian Materiil"> 
                                                            </div> 
                                                        </div> 
                                                    </div> 
                                                    <div class="row"> 
                                                        <div class="col-md-4"> 
                                                            <div class="form-group"> 
                                                                <label for="field-4" class="control-label">Satuan</label> 
                                                                <input type="text" class="form-control" id="field-4" placeholder="Unit Satuan Materiil"> 
                                                            </div> 
                                                        </div> 
                                                        <div class="col-md-4"> 
                                                            <div class="form-group"> 
                                                                <label for="field-5" class="control-label">Merek</label> 
                                                                <input type="text" class="form-control" id="field-5" placeholder="Merek Jika Ada"> 
                                                            </div> 
                                                        </div>
                                                        <div class="col-md-4"> 
                                                            <div class="form-group"> 
                                                                <label for="field-5" class="control-label">Lokasi</label> 
                                                                <input type="text" class="form-control" id="field-5" placeholder="Lokasi"> 
                                                            </div> 
                                                        </div>
                                                    </div> 
                                                    <div class="row"> 
                                                        <div class="col-md-4"> 
                                                            <div class="form-group"> 
                                                                <label for="field-5" class="control-label">Saldo</label> 
                                                                <input type="text" class="form-control" id="field-5" placeholder="Saldo Awal"> 
                                                            </div> 
                                                        </div>
                                                        <div class="col-md-4"> 
                                                            <div class="form-group"> 
                                                                <label for="field-5" class="control-label">Kondisi</label> 
                                                                <input type="text" class="form-control" id="field-5" placeholder="Kondisi"> 
                                                            </div> 
                                                        </div>
                                                    </div> 
                                                </div> 
                                                <div class="modal-footer"> 
                                                    <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Batal</button> 
                                                    <button type="button" class="btn btn-info waves-effect waves-light">Simpan</button> 
                                                </div> 
                                            </div> 
                                        </div>
                                    </div>
								</div>
							</div>
						</div>
						
						

                    </div> <!-- container -->
                               
                </div> <!-- content -->

                <footer class="footer">
                    © 2016. All rights reserved.
                </footer>

            </div>
            <!-- ============================================================== -->
            <!-- End Right content here -->
            <!-- ============================================================== -->


            <!-- Right Sidebar -->
            <div class="side-bar right-bar nicescroll">
                <h4 class="text-center">Chat</h4>
                <div class="contact-list nicescroll">
                    <ul class="list-group contacts-list">
                        <li class="list-group-item">
                            <a href="#">
                                <div class="avatar">
                                    <img src="assets/images/users/avatar-1.jpg" alt="">
                                </div>
                                <span class="name">Chadengle</span>
                                <i class="fa fa-circle online"></i>
                            </a>
                            <span class="clearfix"></span>
                        </li>
                        <li class="list-group-item">
                            <a href="#">
                                <div class="avatar">
                                    <img src="assets/images/users/avatar-2.jpg" alt="">
                                </div>
                                <span class="name">Tomaslau</span>
                                <i class="fa fa-circle online"></i>
                            </a>
                            <span class="clearfix"></span>
                        </li>
                        <li class="list-group-item">
                            <a href="#">
                                <div class="avatar">
                                    <img src="assets/images/users/avatar-3.jpg" alt="">
                                </div>
                                <span class="name">Stillnotdavid</span>
                                <i class="fa fa-circle online"></i>
                            </a>
                            <span class="clearfix"></span>
                        </li>
                        <li class="list-group-item">
                            <a href="#">
                                <div class="avatar">
                                    <img src="assets/images/users/avatar-4.jpg" alt="">
                                </div>
                                <span class="name">Kurafire</span>
                                <i class="fa fa-circle online"></i>
                            </a>
                            <span class="clearfix"></span>
                        </li>
                        <li class="list-group-item">
                            <a href="#">
                                <div class="avatar">
                                    <img src="assets/images/users/avatar-5.jpg" alt="">
                                </div>
                                <span class="name">Shahedk</span>
                                <i class="fa fa-circle away"></i>
                            </a>
                            <span class="clearfix"></span>
                        </li>
                        <li class="list-group-item">
                            <a href="#">
                                <div class="avatar">
                                    <img src="assets/images/users/avatar-6.jpg" alt="">
                                </div>
                                <span class="name">Adhamdannaway</span>
                                <i class="fa fa-circle away"></i>
                            </a>
                            <span class="clearfix"></span>
                        </li>
                        <li class="list-group-item">
                            <a href="#">
                                <div class="avatar">
                                    <img src="assets/images/users/avatar-7.jpg" alt="">
                                </div>
                                <span class="name">Ok</span>
                                <i class="fa fa-circle away"></i>
                            </a>
                            <span class="clearfix"></span>
                        </li>
                        <li class="list-group-item">
                            <a href="#">
                                <div class="avatar">
                                    <img src="assets/images/users/avatar-8.jpg" alt="">
                                </div>
                                <span class="name">Arashasghari</span>
                                <i class="fa fa-circle offline"></i>
                            </a>
                            <span class="clearfix"></span>
                        </li>
                        <li class="list-group-item">
                            <a href="#">
                                <div class="avatar">
                                    <img src="assets/images/users/avatar-9.jpg" alt="">
                                </div>
                                <span class="name">Joshaustin</span>
                                <i class="fa fa-circle offline"></i>
                            </a>
                            <span class="clearfix"></span>
                        </li>
                        <li class="list-group-item">
                            <a href="#">
                                <div class="avatar">
                                    <img src="assets/images/users/avatar-10.jpg" alt="">
                                </div>
                                <span class="name">Sortino</span>
                                <i class="fa fa-circle offline"></i>
                            </a>
                            <span class="clearfix"></span>
                        </li>
                    </ul>  
                </div>
            </div>
            <!-- /Right-bar -->


        </div>
        <!-- END wrapper -->
    
        <script>
            var resizefunc = [];
        </script>

        <!-- jQuery  -->
        <script src="<?php echo base_url('assets/js/jquery.min.js'); ?>"></script>
        <script src="<?php echo base_url('assets/js/bootstrap.min.js'); ?>"></script>
        <script src="<?php echo base_url('assets/js/detect.js'); ?>"></script>
        <script src="<?php echo base_url('assets/js/fastclick.js'); ?>"></script>
        <script src="<?php echo base_url('assets/js/jquery.slimscroll.js'); ?>"></script>
        <script src="<?php echo base_url('assets/js/jquery.blockUI.js'); ?>"></script>
        <script src="<?php echo base_url('assets/js/waves.js'); ?>"></script>
        <script src="<?php echo base_url('assets/js/wow.min.js'); ?>"></script>
        <script src="<?php echo base_url('assets/js/jquery.nicescroll.js'); ?>"></script>
        <script src="<?php echo base_url('assets/js/jquery.scrollTo.min.js'); ?>"></script>

        <script src="<?php echo base_url('assets/plugins/bootstrap-table/js/bootstrap-table.min.js'); ?>"></script>

        <script src="<?php echo base_url('assets/pages/jquery.bs-table.js'); ?>"></script>


        <script src="<?php echo base_url('assets/js/jquery.core.js'); ?>"></script>
        <script src="<?php echo base_url('assets/js/jquery.app.js'); ?>"></script>
        

	
	</body>
</html>