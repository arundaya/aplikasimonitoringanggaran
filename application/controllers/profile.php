<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class profile extends CI_Controller {
    
     function __Construct(){
        parent::__Construct ();
        $this->load->database(); // load database
        $this->load->model('SismetModel'); // load model 
      }
    
    public function index()
	{
            $cek = $this->session->userdata('logged_in');
            if(empty($cek))
            {
		//$this->load->view('login_view');
               header('location:'.base_url());
            }
            else 
            {
                $st = $this->session->userdata('stts');
                if($st=='satker')
                {
                    //header('location'.base_url.'index.php/dashboard');
                    //$this->data['posts'] = $this->SismetModel->getDataSatker($this->session->userdata('Satker'));
                    $this->load->view('profile');
                }
                else if ($st=='admin')
                {
                     //$this->data['posts'] = $this->SismetModel->getPosts();
                     $this->load->view('profile');
                    //header('location'.base_url.'index.php/admin');
                }
                
            }
        }
        
    public function gantifoto()
	{
            $cek = $this->session->userdata('logged_in');
            if(empty($cek))
            {
		header('location:'.base_url());
            }
            else 
            {
                $st = $this->session->userdata('stts');
                if($st=='satker')
                {
                    $this->load->view('usersetting');
                }
                else if ($st=='admin')
                {
                    $this->load->view('usersetting');
                }
                
            }
        }
        
     
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */