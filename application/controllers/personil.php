<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Personil extends CI_Controller {
        
        function __Construct(){
        parent::__Construct ();
        $this->load->database(); // load database
        //$this->load->model('ProgramModel'); // load model 
      }
      
    
      

    public function index()
	{
            $cek = $this->session->userdata('logged_in');
            if(empty($cek))
            {
		//$this->load->view('login_view');
               header('location:'.base_url());
            }
            else 
            {
                $st = $this->session->userdata('stts');
                if($st=='satker')
                {
                    //header('location'.base_url.'index.php/dashboard');
                    $this->load->view('datapersoniltni');
                    
                }
                else if ($st=='adminroren')
                {
                    //header('location'.base_url.'index.php/adminroren');
                    $this->load->view('datapersoniltni');
                }
                 else if ($st=='admin')
                {
                    //$this->data['posts'] = $this->ProgramModel->getPosts();
                    //$this->data['rkakl'] = $this->ProgramModel->getDataRKAKL();
                    
                    $this->load->view('datapersoniltni');
                    //$this->load->view('dataprogram1');
                    //header('location'.base_url.'index.php/admin');
                }
                
            }
        }
        
    public function viewlaporangajitni()
        {
            $cek = $this->session->userdata('logged_in');
            if(empty($cek))
            {
		//$this->load->view('login_view');
               header('location:'.base_url());
            }
            else 
            {
                $st = $this->session->userdata('stts');
                if($st=='satker')
                {
                    //header('location'.base_url.'index.php/dashboard');
                    $this->load->view('lapdaftargajitni');
                    
                }
                else if ($st=='adminroren')
                {
                    //header('location'.base_url.'index.php/adminroren');
                    $this->load->view('lapdaftargajitni');
                }
                 else if ($st=='admin')
                {
                    //$this->data['posts'] = $this->ProgramModel->getPosts();
                    //$this->data['rkakl'] = $this->ProgramModel->getDataRKAKL();
                    
                    $this->load->view('lapdaftargajitni');
                    //$this->load->view('dataprogram1');
                    //header('location'.base_url.'index.php/admin');
                }
                
            }
        }
        
        public function cetakdatapersoniltni()
        {
            $cek = $this->session->userdata('logged_in');
            if(empty($cek))
            {
		//$this->load->view('login_view');
               header('location:'.base_url());
            }
            else 
            {
                ob_start();
                //$this->data['rkakl'] = $this->ProgramModel->getDataRKAKL();
                //$this->load->view('printdataprogram', $this->data);
                $this->load->view('printdatapersoniltni');
                $html = ob_get_contents();
                ob_end_clean();

                require_once('./assets/html2pdf/html2pdf.class.php');
                $width_in_mm = 8.27 * 25.4; 
                $height_in_mm = 12.99 * 25.4;
                $pdf = new HTML2PDF('P',array($width_in_mm,$height_in_mm),'en');
                $pdf->WriteHTML($html);
                $pdf->Output('cetakdatagajipersoniltni.pdf');
                
            }
        }
        
        public function cetakdaftgajitni()
        {
            $cek = $this->session->userdata('logged_in');
            if(empty($cek))
            {
		//$this->load->view('login_view');
               header('location:'.base_url());
            }
            else 
            {
                ob_start();
                //$this->data['rkakl'] = $this->ProgramModel->getDataRKAKL();
                //$this->load->view('printdataprogram', $this->data);
                $this->load->view('printdatagajipersoniltni');
                $html = ob_get_contents();
                ob_end_clean();

                require_once('./assets/html2pdf/html2pdf.class.php');
                $width_in_mm = 8.27 * 25.4; 
                $height_in_mm = 12.99 * 25.4;
                $pdf = new HTML2PDF('L',array($width_in_mm,$height_in_mm),'en');
                $pdf->WriteHTML($html);
                $pdf->Output('cetakdatagajipersoniltni.pdf');
                
            }
        }
    
}


/* End of file welcome.php */