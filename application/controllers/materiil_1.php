<?php 

class materiil extends CI_Controller {
        
      function __Construct(){
        parent::__Construct ();
        $this->load->database(); // load database
        $this->load->library('datatables');
        $this->load->model('MateriilModel'); // load model 
      }


        public function index()
	{
            header('location:'.base_url());
        }
        
       public function tabelmateriil()
	{
            $cek = $this->session->userdata('logged_in');
            if(empty($cek))
            {
		header('location:'.base_url());
            }
            else 
            {
                $st = $this->session->userdata('stts');
                if($st=='satker')
                {
                    //header('location'.base_url.'index.php/dashboard');
                    $this->load->view('datamateriil');
                    
                }
                else if ($st=='admin')
                {
                    //$this->data['posts'] = $this->ProgramModel->getPosts();
                    //$this->data['rkakl'] = $this->ProgramModel->getDataRKAKL();
                    //$this->data['materiil'] = $this->MateriilModel->getDataMateriil();
                    $this->load->view('datamateriil');
                    //header('location'.base_url.'index.php/admin');
                }
                
            }
        }
        
        function json() {
        header('Content-Type: application/json');
        echo $this->MateriilModel->json();
        }
    function json2() {
        header('Content-Type: application/json');
        echo $this->MateriilModel->json2();
        }
        
        public function tabelsatuan()
	{
            $cek = $this->session->userdata('logged_in');
            if(empty($cek))
            {
		//$this->load->view('login_view');
               header('location:'.base_url());
            }
            else 
            {
                $st = $this->session->userdata('stts');
                if($st=='satker')
                {
                    //header('location'.base_url.'index.php/dashboard');
                    $this->load->view('datasatuan');
                    
                }
                else if ($st=='adminroren')
                {
                    //header('location'.base_url.'index.php/adminroren');
                    $this->load->view('datasatuan');
                }
                 else if ($st=='admin')
                {
                    //$this->data['posts'] = $this->ProgramModel->getPosts();
                    //$this->data['rkakl'] = $this->ProgramModel->getDataRKAKL();
                    //$this->data['rkakl2016'] = $this->ProgramModel->getDataRKAKL2016();
                    $this->load->view('datasatuan');
                    //header('location'.base_url.'index.php/admin');
                }
                
            }
        }
        
        public function tabelmerek()
	{
            $cek = $this->session->userdata('logged_in');
            if(empty($cek))
            {
		//$this->load->view('login_view');
               header('location:'.base_url());
            }
            else 
            {
                $st = $this->session->userdata('stts');
                if($st=='satker')
                {
                    //header('location'.base_url.'index.php/dashboard');
                    $this->load->view('datamerek');
                    
                }
                else if ($st=='adminroren')
                {
                    //header('location'.base_url.'index.php/adminroren');
                    $this->load->view('datamerek');
                }
                 else if ($st=='admin')
                {
                    //$this->data['posts'] = $this->ProgramModel->getPosts();
                    //$this->data['rkakl'] = $this->ProgramModel->getDataRKAKL();
                    //$this->data['rkakl2016'] = $this->ProgramModel->getDataRKAKL2016();
                    $this->load->view('datamerek');
                    //header('location'.base_url.'index.php/admin');
                }
                
            }
        }
        
        public function tabellokasi()
	{
            $cek = $this->session->userdata('logged_in');
            if(empty($cek))
            {
		//$this->load->view('login_view');
               header('location:'.base_url());
            }
            else 
            {
                $st = $this->session->userdata('stts');
                if($st=='satker')
                {
                    //header('location'.base_url.'index.php/dashboard');
                    $this->load->view('datalokasi');
                    
                }
                else if ($st=='adminroren')
                {
                    //header('location'.base_url.'index.php/adminroren');
                    $this->load->view('datalokasi');
                }
                 else if ($st=='admin')
                {
                    //$this->data['posts'] = $this->ProgramModel->getPosts();
                    //$this->data['rkakl'] = $this->ProgramModel->getDataRKAKL();
                    //$this->data['rkakl2016'] = $this->ProgramModel->getDataRKAKL2016();
                    $this->load->view('datalokasi');
                    //header('location'.base_url.'index.php/admin');
                }
                
            }
        }
        
        
        
        public function saldoawal()
	{
            $cek = $this->session->userdata('logged_in');
            if(empty($cek))
            {
                $redirect_to = base_url('/materiil/saldoawal');
                $this->session->set_userdata('redirect_to',$redirect_to);
		header('location:'.base_url());
            }
            else 
            {
                $st = $this->session->userdata('stts');
                if($st=='satker')
                {
                    $this->load->view('transsaldoawal');
                    
                }
                else if ($st=='adminroren')
                {
                    //header('location'.base_url.'index.php/adminroren');
                    $this->load->view('transsaldoawal');
                }
                 else if ($st=='admin')
                {
                    //$this->data['materiil'] = $this->MateriilModel->getSaldoAwal();
                    $this->load->view('transsaldoawal');
                    
                }
                
            }
        }
        
        public function tambahsaldoawal()
	{
            $cek = $this->session->userdata('logged_in');
            if(empty($cek))
            {
                $redirect_to = base_url('/materiil/tambahsaldoawal');
                $this->session->set_userdata('redirect_to',$redirect_to);
		header('location:'.base_url());
            }
            else 
            {
                $st = $this->session->userdata('stts');
                if($st=='satker')
                {
                    $this->load->view('tambahsaldoawal');
                    
                }
                else if ($st=='admin')
                {
                    //$this->data['materiil'] = $this->MateriilModel->getDataMateriil();
                    $this->load->view('tambahsaldoawal');
                    
                }
                
            }
        }
        
        public function pembelian()
	{
            $cek = $this->session->userdata('logged_in');
            if(empty($cek))
            {
                $redirect_to = base_url('/materiil/pembelian');
                $this->session->set_userdata('redirect_to',$redirect_to);
                header('location:'.base_url());
            }
            else 
            {
                $st = $this->session->userdata('stts');
                if($st=='satker')
                {
                    $this->load->view('tabelpembelian');
                }
                else if ($st=='admin')
                {
                    //$this->data['posts'] = $this->ProgramModel->getPosts();
                    //$this->data['rkakl'] = $this->ProgramModel->getDataRKAKL();
                    //$this->data['materiil'] = $this->MateriilModel->getDataPembelian();
                    $this->load->view('transpembelian');
                    //header('location'.base_url.'index.php/admin');
                }
                
            }
        }
        
        public function hibah()
	{
            $cek = $this->session->userdata('logged_in');
            if(empty($cek))
            {
		//$this->load->view('login_view');
               header('location:'.base_url());
            }
            else 
            {
                $st = $this->session->userdata('stts');
                if($st=='satker')
                {
                    //header('location'.base_url.'index.php/dashboard');
                    $this->load->view('transhibah');
                    
                }
                else if ($st=='adminroren')
                {
                    //header('location'.base_url.'index.php/adminroren');
                    $this->load->view('transhibah');
                }
                 else if ($st=='admin')
                {
                    //$this->data['posts'] = $this->ProgramModel->getPosts();
                    //$this->data['rkakl'] = $this->ProgramModel->getDataRKAKL();
                    //$this->data['materiil'] = $this->MateriilModel->getDataHibah();
                    $this->load->view('transhibah');
                    //header('location'.base_url.'index.php/admin');
                }
                
            }
        }
        
        public function pengurangan()
	{
            $cek = $this->session->userdata('logged_in');
            if(empty($cek))
            {
		header('location:'.base_url());
            }
            else 
            {
                $st = $this->session->userdata('stts');
                if($st=='satker')
                {
                    
                    $this->load->view('transpenghapusanmateriil');
                    
                }
                else if ($st=='admin')
                {
                   
                   //$this->data['materiil'] = $this->MateriilModel->getPenguranganMateriil();
                    $this->load->view('transpenghapusanmateriil');
                }
                
            }
        }
        
        public function perubahankondisi()
	{
            $cek = $this->session->userdata('logged_in');
            if(empty($cek))
            {
		//$this->load->view('login_view');
               header('location:'.base_url());
            }
            else 
            {
                $st = $this->session->userdata('stts');
                if($st=='satker')
                {
                    //header('location'.base_url.'index.php/dashboard');
                    $this->load->view('perubahanmateriil');
                    
                }
                else if ($st=='adminroren')
                {
                    //header('location'.base_url.'index.php/adminroren');
                    $this->load->view('perubahanmateriil');
                }
                 else if ($st=='admin')
                {
                    //$this->data['posts'] = $this->ProgramModel->getPosts();
                    //$this->data['rkakl'] = $this->ProgramModel->getDataRKAKL();
                    $this->data['materiil'] = $this->MateriilModel->getDataMateriil();
                    $this->load->view('perubahanmateriil', $this->data);
                    //header('location'.base_url.'index.php/admin');
                }
                
            }
        }
        
        public function printtranhibah()
        {
          $cek = $this->session->userdata('logged_in');
            if(empty($cek))
            {
		//$this->load->view('login_view');
               header('location:'.base_url());
            }
            else 
            {
                $data['materiil'] = [];
                $html=$this->load->view('printtranshibah', $data, true);
                
                $this->load->library('m_pdf');
                $this->m_pdf->pdf = new mPDF();
                $this->m_pdf->pdf->AddPage('L', // L - landscape, P - portrait
            '', '', '', '',
            15, // margin_left
            15, // margin right
            34, // margin top
            25, // margin bottom
            13, // margin header
            12); // margin footer
               //generate the PDF from the given html
                $header = '
<table style="width: 100%; border: none">
          <tr >
              <td>
                  <h3>KEMENTERIAN : (012) KEMENTERIAN PERTAHANAN</h3>
                  
              </td>
              <td style="text-align:right">
                  <h3>REGISTER TRANSAKSI</h3>
                  
              </td>
              
              
          </tr>
          <tr>
            <td>
                  <h4>SATKER : BIRO PERENCANAAN KEMHAN</h4>
                  <br/>
              </td>
              <td style="text-align:right">
                  <h4>Transaksi Hibah</h4>
                  
              </td>
          </tr>
          <tr>
            <td>
                  
              </td>
              <td style="text-align:right">
                  <h6>Tahun Anggaran : 2016 Semester : 2</h6>
                  
              </td>
          </tr>
      </table>';
                
                $footer = '<table style="width: 100%; border: none">
          <tr >
              <td width="650">
                  <hr>
              </td>
          </tr>
          <tr >
              <td>
                  Halaman {PAGENO} / {nb}
              </td>
          </tr>
      </table>';
                
                $this->m_pdf->pdf->SetHTMLHeader($header, '',TRUE);
                $this->m_pdf->pdf->SetHTMLFooter($footer);
                $this->m_pdf->pdf->WriteHTML($html);

                //download it.
                $this->m_pdf->pdf->Output($pdfFilePath, "I"); 
                
            }
        }
        
        public function printtranpembelian()
        {
          $cek = $this->session->userdata('logged_in');
            if(empty($cek))
            {
		//$this->load->view('login_view');
               header('location:'.base_url());
            }
            else 
            {
                $data['materiil'] = [];
                $html=$this->load->view('printtranspembelian', $data, true);
                
                $this->load->library('m_pdf');
                $this->m_pdf->pdf = new mPDF();
                $this->m_pdf->pdf->AddPage('L', // L - landscape, P - portrait
            '', '', '', '',
            15, // margin_left
            15, // margin right
            34, // margin top
            25, // margin bottom
            13, // margin header
            12); // margin footer
               //generate the PDF from the given html
                $header = '
<table style="width: 100%; border: none">
          <tr >
              <td>
                  <h3>KEMENTERIAN : (012) KEMENTERIAN PERTAHANAN</h3>
                  
              </td>
              <td style="text-align:right">
                  <h3>REGISTER TRANSAKSI</h3>
                  
              </td>
              
              
          </tr>
          <tr>
            <td>
                  <h4>SATKER : BIRO PERENCANAAN KEMHAN</h4>
                  <br/>
              </td>
              <td style="text-align:right">
                  <h4>Transaksi Pembelian</h4>
                  
              </td>
          </tr>
          <tr>
            <td>
                  
              </td>
              <td style="text-align:right">
                  <h6>Tahun Anggaran : 2016 Semester : 2</h6>
                  
              </td>
          </tr>
      </table>';
                
                $footer = '<table style="width: 100%; border: none">
          <tr >
              <td width="650">
                  <hr>
              </td>
          </tr>
          <tr >
              <td>
                  Halaman {PAGENO} / {nb}
              </td>
          </tr>
      </table>';
                
                $this->m_pdf->pdf->SetHTMLHeader($header, '',TRUE);
                $this->m_pdf->pdf->SetHTMLFooter($footer);
                $this->m_pdf->pdf->WriteHTML($html);

                //download it.
                $this->m_pdf->pdf->Output($pdfFilePath, "I"); 
                
            }
        }
        
        public function printtransaldoawal()
        {
          $cek = $this->session->userdata('logged_in');
            if(empty($cek))
            {
		//$this->load->view('login_view');
               header('location:'.base_url());
            }
            else 
            {
                $data['materiil'] = [];
                $html=$this->load->view('printtranssaldoawal', $data, true);
                
                $this->load->library('m_pdf');
                $this->m_pdf->pdf = new mPDF();
                $this->m_pdf->pdf->AddPage('L', // L - landscape, P - portrait
            '', '', '', '',
            15, // margin_left
            15, // margin right
            34, // margin top
            25, // margin bottom
            13, // margin header
            12); // margin footer
               //generate the PDF from the given html
                $header = '
<table style="width: 100%; border: none">
          <tr >
              <td>
                  <h3>KEMENTERIAN : (012) KEMENTERIAN PERTAHANAN</h3>
                  
              </td>
              <td style="text-align:right">
                  <h3>REGISTER TRANSAKSI</h3>
                  
              </td>
              
              
          </tr>
          <tr>
            <td>
                  <h4>SATKER : BIRO PERENCANAAN KEMHAN</h4>
                  <br/>
              </td>
              <td style="text-align:right">
                  <h4>Saldo Awal</h4>
                  
              </td>
          </tr>
          <tr>
            <td>
                  
              </td>
              <td style="text-align:right">
                  <h6>Tahun Anggaran : 2016 Semester : 2</h6>
                  
              </td>
          </tr>
      </table>';
                
                $footer = '<table style="width: 100%; border: none">
          <tr >
              <td width="650">
                  <hr>
              </td>
          </tr>
          <tr >
              <td>
                  Halaman {PAGENO} / {nb}
              </td>
          </tr>
      </table>';
                
                $this->m_pdf->pdf->SetHTMLHeader($header, '',TRUE);
                $this->m_pdf->pdf->SetHTMLFooter($footer);
                $this->m_pdf->pdf->WriteHTML($html);

                //download it.
                $this->m_pdf->pdf->Output($pdfFilePath, "I"); 
                
            }
        }
        
        public function printdaftarmateriil()
        {
          set_time_limit('1000');
          $cek = $this->session->userdata('logged_in');
            if(empty($cek))
            {
		//$this->load->view('login_view');
               header('location:'.base_url());
            }
            else 
            {
                //tcpdf-----
//                ob_start();
//                
//                $this->load->library('Pdf');
//                $pdf = new Pdf('P', 'mm', 'A4', true, 'UTF-8', false);
//                $pdf->SetTitle('Pdf Example');
//                $pdf->SetHeaderMargin(30);
//                $pdf->SetTopMargin(20);
//                $pdf->setFooterMargin(20);
//                $pdf->SetAutoPageBreak(true);
//                $pdf->SetAuthor('Author');
//                $pdf->SetDisplayMode('real', 'default');
//                // set default font subsetting mode
//                $pdf->setFontSubsetting(false);
//
//                // Set font
//                // dejavusans is a UTF-8 Unicode font, if you only need to
//                // print standard ASCII chars, you can use core fonts like
//                // helvetica or times to reduce file size.
//                $pdf->SetFont('dejavusans', '', 14, '', true);
//
//                // Add a page
//                // This method has several options, check the source code documentation for more information.
//                $pdf->AddPage();
//                
//                $this->data['materiil'] = $this->MateriilModel->getDataMateriil();
//                $this->load->view('printlapdaftarmateriil', $this->data);
//                $html = ob_get_contents();
//                ob_end_clean();
//
//                // set text shadow effect
//                $pdf->setTextShadow(array('enabled'=>true, 'depth_w'=>0.2, 'depth_h'=>0.2, 'color'=>array(196,196,196), 'opacity'=>1, 'blend_mode'=>'Normal'));
//
//
//                $pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);
//                $pdf->Output('pdfexample.pdf', 'I');
                
//                //mPDF
                $data['materiil'] = $this->MateriilModel->getDataMateriil();
                //load the view and saved it into $html variable
                
                
                $html=$this->load->view('printlapdaftarmateriil', $data, true);

                //this the the PDF filename that user will get to download
                $pdfFilePath = "output_pdf_name.pdf";

                //load mPDF library
               
                $this->load->library('m_pdf');
                $this->m_pdf->pdf = new mPDF();
                $this->m_pdf->pdf->AddPage('P', // L - landscape, P - portrait
            '', '', '', '',
            15, // margin_left
            15, // margin right
            30, // margin top
            25, // margin bottom
            18, // margin header
            12); // margin footer
               //generate the PDF from the given html
                $header = '
<table style="width: 100%; border: none">
          <tr >
              <td>
                  <h3>DAFTAR KODE MATERIIL</h3>
              </td>
          </tr>
      </table>';
                
                $footer = '<table style="width: 100%; border: none">
          <tr >
              <td width="650">
                  <hr>
              </td>
          </tr>
          <tr >
              <td>
                  Halaman {PAGENO} / {nb}
              </td>
          </tr>
      </table>';
                
                $this->m_pdf->pdf->SetHTMLHeader($header, '',TRUE);
                $this->m_pdf->pdf->SetHTMLFooter($footer);
                $this->m_pdf->pdf->WriteHTML($html);

                //download it.
                $this->m_pdf->pdf->Output($pdfFilePath, "I");    
                //$this->m_pdf->pdf->Output($pdfFilePath, "D");    

                
                //html2pdf
//                ob_start();
//                $this->data['materiil'] = $this->MateriilModel->getDataMateriil();
//                $this->load->view('printlapdaftarmateriil', $this->data);
//                $html = ob_get_contents();
//                ob_end_clean();
//
//                require_once('./asset/html2pdf/html2pdf.class.php');
//                $pdf = new HTML2PDF('P','A4','en');
//                $pdf->WriteHTML($html);
//                $pdf->Output('LaporanDaftarMateriil.pdf');
                
            }
        }
        
         public function printlapmateriil()
        {
          $cek = $this->session->userdata('logged_in');
            if(empty($cek))
            {
		//$this->load->view('login_view');
               header('location:'.base_url());
            }
            else 
            {
                $data['materiil'] = [];
                $html=$this->load->view('printlapmateriil', $data, true);
                
                $this->load->library('m_pdf');
                $this->m_pdf->pdf = new mPDF();
                $this->m_pdf->pdf->AddPage('L', // L - landscape, P - portrait
            '', '', '', '',
            15, // margin_left
            15, // margin right
            40, // margin top
            25, // margin bottom
            13, // margin header
            12); // margin footer
               //generate the PDF from the given html
                $header = '
<table style="width: 100%; border: none">
          <tr >
              <td>
                  <h5>KEMENTERIAN : (012) KEMENTERIAN PERTAHANAN</h5>
                  
              </td>
              <td style="text-align:right">
                  
                  
              </td>
              
              
          </tr>
          <tr>
            <td colspan="2" style="text-align:center">
                  <h5>LAPORAN DATA MATERIIL</h5>
                  <br/>
              </td>
             
          </tr>
          <tr>
            <td colspan="2" style="text-align:center">
                  <h5>RINCIAN KELOMPOK MATERIIL</h5>
                  <br/>
              </td>
             
          </tr>
          <tr>
            <td colspan="2" style="text-align:center">
                  <h5>SEMESTER 1 T.A. 2016</h5>
                  <br/>
              </td>
             
          </tr>
          <tr>
            <td>
                  <h6>SATKER : ROREN</h6>
              </td>
              <td style="text-align:right">
                  <h6>Halaman {PAGENO} / {nb}</h6>
                  
              </td>
          </tr>
      </table>';
                
                $footer = '<table style="width: 100%; border: none">
          <tr >
              <td width="650">
                  <hr>
              </td>
          </tr>
          <tr >
              <td style="font-size:9">
                  Halaman {PAGENO} / {nb}
              </td>
          </tr>
      </table>';
                
                $this->m_pdf->pdf->SetHTMLHeader($header, '',TRUE);
                $this->m_pdf->pdf->SetHTMLFooter($footer);
                $this->m_pdf->pdf->WriteHTML($html);

                //download it.
                $this->m_pdf->pdf->Output($pdfFilePath, "I"); 
                
            }
        }
        
         public function printlapkondisimateriil()
        {
          $cek = $this->session->userdata('logged_in');
            if(empty($cek))
            {
		//$this->load->view('login_view');
               header('location:'.base_url());
            }
            else 
            {
                $data['materiil'] = [];
                $html=$this->load->view('printlapkondisi', $data, true);
                
                $this->load->library('m_pdf');
                $this->m_pdf->pdf = new mPDF();
                $this->m_pdf->pdf->AddPage('L', // L - landscape, P - portrait
            '', '', '', '',
            15, // margin_left
            15, // margin right
            40, // margin top
            25, // margin bottom
            13, // margin header
            12); // margin footer
               //generate the PDF from the given html
                $header = '
<table style="width: 100%; border: none">
          <tr >
              <td colspan="2" style="text-align:center">
                  <h5>LAPORAN KONDISI MATERIIL</h5>
                  
              </td>
              
              
              
          </tr>
         
          <tr>
            <td colspan="2" style="text-align:center">
                  <h5>SEMESTER 1 T.A. 2016</h5>
                  <br/>
              </td>
             
          </tr>
          <tr>
            <td>
                  <h6>SATKER : ROREN</h6>
              </td>
              <td style="text-align:right">
                  <h6>Halaman {PAGENO} / {nb}</h6>
                  
              </td>
          </tr>
      </table>';
                
                $footer = '<table style="width: 100%; border: none">
          <tr >
              <td width="650">
                  <hr>
              </td>
          </tr>
          <tr >
              <td style="font-size:9">
                  Halaman {PAGENO} / {nb}
              </td>
          </tr>
      </table>';
                
                $this->m_pdf->pdf->SetHTMLHeader($header, '',TRUE);
                $this->m_pdf->pdf->SetHTMLFooter($footer);
                $this->m_pdf->pdf->WriteHTML($html);

                //download it.
                $this->m_pdf->pdf->Output($pdfFilePath, "I"); 
                
            }
        }
        
        public function laporandatamateriil()
	{
            $cek = $this->session->userdata('logged_in');
            if(empty($cek))
            {
		//$this->load->view('login_view');
               header('location:'.base_url());
            }
            else 
            {
                $st = $this->session->userdata('stts');
                if($st=='satker')
                {
                    //header('location'.base_url.'index.php/dashboard');
                    $this->load->view('lapmateriil');
                    
                }
                else if ($st=='adminroren')
                {
                    //header('location'.base_url.'index.php/adminroren');
                    $this->load->view('lapmateriil');
                }
                 else if ($st=='admin')
                {
                    //$this->data['posts'] = $this->ProgramModel->getPosts();
                    //$this->data['rkakl'] = $this->ProgramModel->getDataRKAKL();
                   //$this->data['materiil'] = $this->MateriilModel->getDataMateriil();
                    $this->load->view('lapmateriil');
                    //header('location'.base_url.'index.php/admin');
                }
                
            }
        }
        
        public function laporankondisi()
	{
            $cek = $this->session->userdata('logged_in');
            if(empty($cek))
            {
		//$this->load->view('login_view');
               header('location:'.base_url());
            }
            else 
            {
                $st = $this->session->userdata('stts');
                if($st=='satker')
                {
                    //header('location'.base_url.'index.php/dashboard');
                    $this->load->view('lapkondisimateriil');
                    
                }
                else if ($st=='adminroren')
                {
                    //header('location'.base_url.'index.php/adminroren');
                    $this->load->view('lapkondisimateriil');
                }
                 else if ($st=='admin')
                {
                    //$this->data['posts'] = $this->ProgramModel->getPosts();
                    //$this->data['rkakl'] = $this->ProgramModel->getDataRKAKL();
                   //$this->data['materiil'] = $this->MateriilModel->getDataMateriil();
                    $this->load->view('lapkondisimateriil');
                    //header('location'.base_url.'index.php/admin');
                }
                
            }
        }
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
