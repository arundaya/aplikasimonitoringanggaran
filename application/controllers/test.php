<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class test extends CI_Controller {
        
        public function _construct()
        {
            session_start();
        }


        public function index()
	{
            $cek = $this->session->userdata('logged_in');
            if(empty($cek))
            {
		$this->load->view('login_view');
            }
            else 
            {
                $st = $this->session->userdata('stts');
                if($st=='satker')
                {
                    //header('location'.base_url.'index.php/dashboard');
                    $this->load->view('test');
                }
                else if ($st=='adminroren')
                {
                    //header('location'.base_url.'index.php/adminroren');
                    $this->load->view('test');
                }
                 else if ($st=='admin')
                {
                    $this->load->view('test');
                    //header('location'.base_url.'index.php/admin');
                }
                
            }
	}
        
        public function login()
        {
            $u = $this->input->post('username');
            $p = $this->input->post('password');
            $s = $this->input->post('satker');
            $this->home_app_model->getLoginData($u, $p, $s);
        }
        
        public function logout()
        {
            $cek = $this->session->userdata('logged_in');
            if(empty($cek))
            {
                header('location:'.base_url());
            }
            else
            {
                $this->session->sess_destroy();
                header('location:'.base_url());
            }
        }
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */