<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class TabelSatuan extends CI_Controller
{
    
    function __Construct(){
        parent::__Construct ();
        $this->load->database(); // load database
        $this->load->model('ProgramModel'); // load model 
      }
      
//    public function _construct()
//        {
//            session_start();
//        }

    public function index()
	{
            $cek = $this->session->userdata('logged_in');
            if(empty($cek))
            {
		//$this->load->view('login_view');
               header('location:'.base_url());
            }
            else 
            {
                $st = $this->session->userdata('stts');
                if($st=='satker')
                {
                    //header('location'.base_url.'index.php/dashboard');
                    $this->load->view('tabelsatuan');
                    
                }
                else if ($st=='adminroren')
                {
                    //header('location'.base_url.'index.php/adminroren');
                    $this->load->view('tabelsatuan');
                }
                 else if ($st=='admin')
                {
                    //$this->data['posts'] = $this->ProgramModel->getPosts();
                    //$this->data['rkakl'] = $this->ProgramModel->getDataRKAKL();
                    //$this->data['rkakl2016'] = $this->ProgramModel->getDataRKAKL2016();
                    $this->load->view('tabelsatuan');
                    //header('location'.base_url.'index.php/admin');
                }
                
            }
        }
        
        
    public function cetak(){
        {
            $cek = $this->session->userdata('logged_in');
            if(empty($cek))
            {
		//$this->load->view('login_view');
               header('location:'.base_url());
            }
            else 
            {
                ob_start();
                $this->data['rkakl'] = $this->ProgramModel->getDataRKAKL();
                $this->load->view('printdataprogram', $this->data);
                $html = ob_get_contents();
                ob_end_clean();

                require_once('./asset/html2pdf/html2pdf.class.php');
                $pdf = new HTML2PDF('L','A4','en');
                $pdf->WriteHTML($html);
                $pdf->Output('dataprogram.pdf');
                
            }
        }
        
        
        
        
    }
        
        
}

