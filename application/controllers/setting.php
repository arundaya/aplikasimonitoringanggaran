<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class setting extends CI_Controller {
        
    function __Construct()
        {
            parent::__Construct ();
            $this->load->database(); // load database
            $this->load->model('SettingModel'); // load model 
        }


    public function index()
	{
            header('location:'.base_url());
        }
        
    public function tabelsatker()
	{
            $this->data['satker'] = $this->SettingModel->getDataSatker();
            $this->load->view('datasatker', $this->data);
        }
        
    public function tabeluser()
	{
            $this->data['user'] = $this->SettingModel->getDataUser();
            $this->load->view('datauser', $this->data);
        }  
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */