<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class LaporanPengurangan extends CI_Controller
{
    
    function __Construct(){
        parent::__Construct ();
        $this->load->database(); // load database
        $this->load->model('PenguranganModel'); // load model 
      }
      
//    public function _construct()
//        {
//            session_start();
//        }

    public function index()
	{
            $cek = $this->session->userdata('logged_in');
            if(empty($cek))
            {
		//$this->load->view('login_view');
               header('location:'.base_url());
            }
            else 
            {
                $st = $this->session->userdata('stts');
                if($st=='satker')
                {
                    //header('location'.base_url.'index.php/dashboard');
                    $this->load->view('laporanpengurangan');
                    
                }
                else if ($st=='adminroren')
                {
                    //header('location'.base_url.'index.php/adminroren');
                    $this->load->view('laporanpengurangan');
                }
                 else if ($st=='admin')
                {
                    //$this->data['posts'] = $this->ProgramModel->getPosts();
                    //$this->data['rkakl'] = $this->ProgramModel->getDataRKAKL();
                    $this->data['materiil'] = $this->PenguranganModel->getDataMateriil();
                    $this->load->view('laporanpengurangan', $this->data);
                    //header('location'.base_url.'index.php/admin');
                }
                
            }
        }
        
        
    public function printlaporan(){
        {
          $cek = $this->session->userdata('logged_in');
            if(empty($cek))
            {
		//$this->load->view('login_view');
               header('location:'.base_url());
            }
            else 
            {
                ob_start();
                $this->data['materiil'] = $this->PenguranganModel->getDataMateriil();
                $this->load->view('printlappengurangan', $this->data);
                $html = ob_get_contents();
                ob_end_clean();

                require_once('./asset/html2pdf/html2pdf.class.php');
                $pdf = new HTML2PDF('L','A4','en');
                $pdf->WriteHTML($html);
                $pdf->Output('LapPengurangan.pdf');
                
            }
        }
        
        
        
        
    }
        
        
}

