<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class sismet extends CI_Controller {
    
     function __Construct(){
        parent::__Construct ();
        $this->load->database(); // load database
        $this->load->model('SismetModel'); // load model 
      }
    
    public function index()
	{
            $cek = $this->session->userdata('logged_in');
            if(empty($cek))
            {
		//$this->load->view('login_view');
               header('location:'.base_url());
            }
            else 
            {
                $st = $this->session->userdata('stts');
                if($st=='satker')
                {
                    //header('location'.base_url.'index.php/dashboard');
                    $this->data['posts'] = $this->SismetModel->getDataSatker($this->session->userdata('Satker'));
                    $this->load->view('datasismet', $this->data);
                }
                else if ($st=='adminroren')
                {
                    //header('location'.base_url.'index.php/adminroren');
                    $this->load->view('datasismet');
                }
                 else if ($st=='admin')
                {
                     $this->data['posts'] = $this->SismetModel->getPosts();
                     $this->load->view('datasismet', $this->data);
                    //header('location'.base_url.'index.php/admin');
                }
                
            }
        }
        
    public function tabelrefjnssismet()
	{
            $cek = $this->session->userdata('logged_in');
            if(empty($cek))
            {
		//$this->load->view('login_view');
               header('location:'.base_url());
            }
            else 
            {
                $st = $this->session->userdata('stts');
                if($st=='satker')
                {
                    //header('location'.base_url.'index.php/dashboard');
                    $this->load->view('datarefjnsmateriil');
                    
                }
                else if ($st=='adminroren')
                {
                    //header('location'.base_url.'index.php/adminroren');
                    $this->load->view('datarefjnsmateriil');
                }
                 else if ($st=='admin')
                {
                    
                    //$this->data['materiil'] = $this->MateriilModel->getDataMateriil();
                    $this->load->view('datarefjnsmateriil');
                    //header('location'.base_url.'index.php/admin');
                }
                
            }
        }
        
    public function submit()
    {
        
            //get the form data
            $data = array (
                 "NomorSismet" => $this->input->post('inpnomor'),
                 "JenisSismet" => $this->input->post('inputJenis'),
                 
                "Judul" => $this->input->post('inputJudul'),
                 "PaguAnggaran" => $this->input->post('inputPagu'), 
             );
            
            $this->db->insert('tblsismet', $data);
             //$this->index;
    }   
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */