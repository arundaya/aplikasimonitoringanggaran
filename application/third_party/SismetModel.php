<?php
class SismetModel extends CI_Model
{
     function getPosts(){
    $this->db->select("NomorSismet,JenisSismet,Judul,PaguAnggaran"); 
    $this->db->from('tblsismet');
    $query = $this->db->get();
    return $query->result();
 }
 
  function getDataSatker($satker){
     
    $query = $this->db->get_where('tblsismet', array('NamaSatker' => $satker));
    
    return $query->result();
 }
 
 function getSave($data){
      $this->db->insert('tblsismet', $data);
      
 }
}