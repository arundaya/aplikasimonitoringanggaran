<?php
// memanggil file config.php
include "../config/Koneksi.php";

/*
 * DataTables example server-side processing script.
 *
 * Please note that this script is intentionally extremely simply to show how
 * server-side processing can be implemented, and probably shouldn't be used as
 * the basis for a large complex system. It is suitable for simple use cases as
 * for learning.
 *
 * See http://datatables.net/usage/server-side for full details on the server-
 * side processing requirements of DataTables.
 *
 * @license MIT - http://datatables.net/license_mit
 */

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * Easy set variables
 */

// DB table to use
$table = 'tbl_anggota';

// Table's primary key
$primaryKey = 'ID';

// Array of database columns which should be read and sent back to DataTables.
// The `db` parameter represents the column name in the database, while the `dt`
// parameter represents the DataTables column identifier. In this case object
// parameter names
$columns = array(
	array( 'db' => 'ID', 'dt' => 0 ),
	array( 'db' => 'NRP','dt'        => 1),
	array( 'db' => 'Nama',  'dt' => 2 ),
	array( 'db' => 'Kelamin', 'dt' => 3,'formatter' => function( $d, $row ) {
            
            if($d == 'L') {
                return 'Laki-Laki';
            }if($d == 'P') {
                return 'Perempuan';
            } else {
                return '-';
            }
        }),
	array( 'db' => 'Tgl_Lahir', 'dt' => 4,'formatter' => function( $d, $row ) {
            
            if($d == '1970-01-01' or $d == '0000-00-00') {
                return '-';
            } else {
                return date( 'd-M-Y', strtotime($d));
            }
        }),
    array( 'db' => 'Pangkat',  'dt' => 5 ),
	array( 'db' => 'NamaSatker',  'dt' => 6 ),
	array( 'db' => 'NamaUO',  'dt' => 7 ),
	array( 'db'        => 'ID','dt'        => 8,'formatter' => function( $d, $row ) {
                $buttons='<div class="hidden-sm hidden-xs action-buttons">
																<a class="blue" href="DaftarAnggota_view.php?ID='.$row[0].'">
																	<i class="ace-icon fa fa-search-plus bigger-130"></i>
																</a>
																<a class="green" href="DaftarAnggota_input.php?ID='.$row[0].'">
																	<i class="ace-icon fa fa-pencil bigger-130"></i>
																</a>

															</div>';
                return $buttons;
            }),
);

// SQL server connection information
$sql_details = array(
	'user' => $dbuser,
	'pass' => $dbpass,
	'db'   => $dbname,
	'host' => $dbhost
);


/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * If you just want to use the basic configuration for DataTables with PHP
 * server-side, there is no need to edit below this line.
 */

require( 'inc/ssp.class.php' );

echo json_encode(
	SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns )
);

